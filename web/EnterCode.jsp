<%-- 
    Document   : EnterCode
    Created on : Feb 26, 2021, 11:36:34 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Enter Code Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <div>
            <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                <div class="container-fluid">
                    <a class="navbar-branch" href="#">
                        <img src= "img/Logo.png" height = "100" alt="#">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </a>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <form action="logout" method="GET">
                                    <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:set var="errorVerify" value="${requestScope.ERRORVERIFY}"/>
        <c:if test="${not empty dtoAccount}">
            <c:set var="fullname" value="${dtoAccount.fullName}"/>
            <div>
                <font color="red">
                Welcome, ${fullname}
                </font>
                <c:if test="${not empty errorROLE}">
                    <font color="red">
                    <br/>${errorROLE}
                    </font>
                </c:if>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-3 col-sm-6 col-xs-12 row-container">
                        <h2>Please input the verify code in you email</h2>
                        <c:set var="errors" value="${requestScope.INSERTERR}"/>
                        <form action = "submitVerifyCode" method="post" class="register-form">
                            <div class="form-group">
                                Verify code: <input type="text" name="txtVerifyCode" value="${param.txtVerifyCode}"/><br/>
                            </div>
                            <input type="hidden" name="accID" value="${dtoAccount.email}"/>
                            <input class="btn btn-success btn-block my-3" type="submit" value="Submit verify Code" name="btAction"/>
                        </form>
                        <c:if test="${not empty errorVerify}">
                            <h1>
                                <font color="red">
                                    ${errorVerify}
                                </font>
                            </h1>
                        </c:if>
                    </div>
                </div>
            </div>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Home</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login !!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
