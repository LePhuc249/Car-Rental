function goToLoginPage() {
    window.location.href = "/CarRental/loginPage";
}

function goToCreateAccountPage() {
    window.location.href = "/CarRental/createAccountPage";
}

function goToForCustomerPage() {
    window.location.href = "/CarRental/forCustomerPage";
}

function goBack() {
    window.history.back();
}

function checkNumFunction() {
    var x, text;
    x = document.getElementById("quantityNum").value;
    if (isNaN(x) || x < 1 || x > 99999999999) {
        alert('Not a number');
    }
}

function ConfirmFunction() {
    var r = confirm("Choose yes if you want to delete!");
    if (r == true) {
        return noCheck();
    } else {
        return false;
    }
}