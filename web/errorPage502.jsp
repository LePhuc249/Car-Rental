<%-- 
    Document   : ErrorPage
    Created on : Jan 22, 2021, 9:13:06 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:if test="${not empty dtoAccount}">
            <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
            <c:if test="${roleAccount eq 'Admin'}">
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <form action="logout" method="GET">
                                    <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                </form>
                            </li>
                        </ul>
                    </div>
                </nav>
                <h1 class="error-label">Bad Gateway!!!!</h1>
                <button class="error-button center-parent center-me" type="button" onclick="goBack()">Click here to return to the previous page</button><br/>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <form action="logout" method="GET">
                                    <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                </form>
                            </li>
                        </ul>
                    </div>
                </nav>
                <h1 class="error-label">Bad Gateway!!!!</h1>
                <button class="error-button center-parent center-me" type="button" onclick="goBack()">Click here to return to the previous page</button><br/>
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                <div class="container-fluid">
                    <a class="navbar-branch" href="#">
                        <img src= "img/Logo.png" height = "100" alt="#">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Home</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <h1 class="error-label">Bad Gateway!!!!</h1>
            <button class="error-button center-parent center-me" type="button" onclick="goBack()">Click here to return to the previous page</button><br/>
        </c:if>
    </body>
</html>
