<%-- 
    Document   : feedbackOrder
    Created on : Feb 6, 2021, 8:59:47 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Feedback Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:set var="errors" value="${requestScope.ERRORTQ}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullName}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="adminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with customer account !!!!!
                    </font>
                </h1>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullName}"/>
                <c:set var="errorROLE" value="${requestScope.errorROLE}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="studentPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="ShowHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                </div>
                <c:set var="sendError" value="${requestScope.ERRORSENDFEEDBACK}"/>
                <c:if test="${not empty sendError}">
                    <font color="red">
                    ${sendError}
                    </font>
                </c:if>
                <div>
                    <c:set var="car" value="${sessionScope.CARFEEDBACK}"/>
                    <c:set var="carCate" value="${sessionScope.CARCATEFEEDBACK}"/>
                    <c:set var="orderDetailID" value="${sessionScope.ORDERLINEID}"/>
                    <c:if test="${not empty car and not empty carCate and not empty orderDetailID}">
                        <form action="submitFeeback" method="POST">
                            Car VIN: ${car.VIN}<br/>
                            Car Type: ${carCate.cateType}<br/>
                            Car Model: ${carCate.cateModel}<br/>
                            Car Product: ${carCate.cateMake}<br/>
                            Car Color: ${carCate.cateColor}<br/>
                            Car Year: ${carCate.cateYear}<br/>
                            Car Price: ${carCate.catePrice}$<br/>
                            <input type="hidden" name="txtOrderLineID" value="${orderDetailID}"/>
                            Comment: <textarea type="textarea" name="txtComment" value="" maxlength="100"></textarea><br/>
                            Score: <input type="text" name="txtNumberQuality" value=""/>/10<br/>
                            <input type="submit" name="btAction" value="Submit feedback"/>
                        </form>
                    </c:if>
                    <c:if test="${empty car or empty carCate or empty orderDetailID}">
                        <h1>
                            <font color="red">
                            Don't have detail to feedback
                            </font>
                        </h1>
                    </c:if>
                </div>    
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Home</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with customer account to use this function!!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
