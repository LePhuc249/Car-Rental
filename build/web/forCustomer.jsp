<%-- 
    Document   : forCustomer
    Created on : Feb 6, 2021, 8:45:32 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Customer Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:set var="errors" value="${requestScope.ERRORTQ}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullName}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forAdminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with customer account !!!!!
                    </font>
                </h1>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullName}"/>
                <c:set var="errorROLE" value="${requestScope.errorROLE}"/>
                <c:set var="searchNoOfPage" value="${sessionScope.NOOFPAGE}"/>
                <c:set var="searchCurrentPage" value="${sessionScope.CURRENTPAGE}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="viewCartPage">View Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                </div>
                <c:set var="listCate" value="${applicationScope.CARTYPELIST}"/>
                <c:set var="errorDate" value="${requestScope.ERRORDATE}"/>
                <c:if test="${not empty listCate}">
                    <div>
                        <h2>Search car page</h2>
                    </div>
                    <div>
                        <form action="search">
                            Car model: <input type="text" name="txtSearchCar" value="${param.txtSearchCar}"/><br/>
                            Date Rental:<input type="text" name="txtDateRental" value="${param.txtDateRental}" style="width:200px;" placeholder="YYYY-MM-DD"/>
                            Date Return:<input type="text" name="txtDateReturn" value="${param.txtDateReturn}" style="width:200px;" placeholder="YYYY-MM-DD"/><br/>
                            <select name="cboCategory">
                                <option value="NULL">-Category-</option>
                                <c:forEach var="category" items="${listCate}">
                                    <option value="${category.typeID}" <c:if test="${param.cboCategory eq category.typeID}">selected="true"</c:if>>${category.typeName}</option>
                                </c:forEach>
                            </select>
                            Car amount: <input type="text" name="txtAmountCar" value="${param.txtAmountCar}"/>
                            <input type="submit" value="Search" name="btAction" />
                        </form>
                        <c:if test="${not empty errorDate}">
                            <h2>
                                <font color="red">
                                ${errorDate}
                                </font>
                            </h2>
                        </c:if>
                    </div>
                    <c:set var="searchCar" value="${param.txtSearchCar}"/>
                    <c:set var="searchCate" value="${param.cboCategory}"/>
                    <c:set var="searchRentalDate" value="${param.txtDateRental}"/>
                    <c:set var="searchReturnDate" value="${param.txtDateReturn}"/>
                    <c:set var="searchAmount" value="${param.txtAmountCar}"/>
                    <c:set var="ERROREMPTY" value="${sessionScope.ERROREMPTYFIELD}"/>
                    <c:if test="${not empty ERROREMPTY}">
                        <h3>
                            <font color="red">
                            ${ERROREMPTY}
                            </font>
                        </h3>
                    </c:if>
                    <c:if test="${empty errorDate}">
                        <c:choose>
                            <c:when test="${not empty searchRentalDate and not empty searchReturnDate and not empty searchAmount and (not empty searchCar or searchCate eq 'NULL')}">
                                <c:set var="listItems" value="${requestScope.SEARCHITEMRESULT}"/>
                                <div>
                                    <c:if test="${not empty listItems}">
                                        <table border="1">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Name</th>
                                                    <th>Color</th>
                                                    <th>Year</th>
                                                    <th>Category</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="items" items="${listItems}" varStatus="counter">
                                                <form action="addToCart" method="POST" class="register-form">
                                                    <tr>
                                                        <td>
                                                            ${counter.count}
                                                        </td>
                                                        <td>
                                                            ${items.cateModel}
                                                            <input type="hidden" name="txtCarCateID" value="${items.cateID}"/>
                                                        </td>
                                                        <td>
                                                            ${items.cateColor}
                                                        </td>
                                                        <td>
                                                            ${items.cateYear}
                                                        </td>
                                                        <td>
                                                            ${items.cateType}
                                                        </td>
                                                        <td>
                                                            ${items.catePrice}$
                                                            <input type="hidden" name="txtCarPrice" value="${items.catePrice}"/>
                                                        </td>
                                                        <td>
                                                            ${items.quantity}
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="page" value="${searchCurrentPage}"/>
                                                            <input type="hidden" name="txtSearchCar" value="${searchCar}"/>
                                                            <input type="hidden" name="cboCategory" value="${searchCate}"/>
                                                            <input type="hidden" name="txtAmount" value="${searchAmount}"/>
                                                            <input type="hidden" name="cartDateRental" value="${searchRentalDate}"/>
                                                            <input type="hidden" name="cartDateReturn" value="${searchReturnDate}"/>
                                                            <input class="btn btn-success" type="submit" value="AddToCart" name="btAction"/>
                                                        </td>
                                                    </tr>
                                                </form>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <c:if test="${not empty searchNoOfPage}">
                                            <table border="1" cellpadding="5" cellspacing="5">
                                                <tr>
                                                    <c:if test="${searchCurrentPage!=1}">
                                                        <c:url var="prevLink" value="search">
                                                            <c:param name="btAction" value="SearchItem"/>
                                                            <c:param name="page" value="${searchCurrentPage-1}"/>
                                                            <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                            <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                            <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                            <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                        </c:url>
                                                        <td><a href="${prevLink}">Previous</a></td>
                                                    </c:if>
                                                    <c:forEach begin="1" end="${searchNoOfPage}" var="i">
                                                        <c:choose>
                                                            <c:when test="${searchCurrentPage eq i}">
                                                                <td>${i}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:url var="pageLink" value="search">
                                                                    <c:param name="btAction" value="SearchItem"/>
                                                                    <c:param name="page" value="${i}"/>
                                                                    <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                    <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                    <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                    <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                    <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                                </c:url>
                                                                <td><a href="${pageLink}">${i}</a></td>
                                                                </c:otherwise>    
                                                            </c:choose>
                                                        </c:forEach>
                                                        <c:if test="${searchCurrentPage < searchNoOfPage}">
                                                            <c:url var="nextLink" value="search">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${searchCurrentPage+1}"/>
                                                                <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                            </c:url>
                                                        <td><a href="${nextLink}">Next</a></td>
                                                    </c:if>            
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${empty listItems}">
                                        <h3>No record matches</h3>
                                    </c:if>
                                </div>
                            </c:when>
                            <c:when test="${not empty searchRentalDate and not empty searchReturnDate and not empty searchAmount and empty searchCar and searchCate ne 'NULL'}">
                                <c:set var="listItems" value="${requestScope.SEARCHITEMRESULT}"/>
                                <div>
                                    <c:if test="${not empty listItems}">
                                        <table border="1">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Name</th>
                                                    <th>Color</th>
                                                    <th>Year</th>
                                                    <th>Category</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="items" items="${listItems}" varStatus="counter">
                                                <form action="addToCart" method="POST" class="register-form">
                                                    <tr>
                                                        <td>
                                                            ${counter.count}
                                                        </td>
                                                        <td>
                                                            ${items.cateModel}
                                                            <input type="hidden" name="txtCarCateID" value="${items.cateID}"/>
                                                        </td>
                                                        <td>
                                                            ${items.cateColor}
                                                        </td>
                                                        <td>
                                                            ${items.cateYear}
                                                        </td>
                                                        <td>
                                                            ${items.cateType}
                                                        </td>
                                                        <td>
                                                            ${items.catePrice}$
                                                            <input type="hidden" name="txtCarPrice" value="${items.catePrice}"/>
                                                        </td>
                                                        <td>
                                                            ${items.quantity}
                                                        </td>
                                                        <td>
                                                            <input class="btn btn-success" type="submit" value="AddToCart" name="btAction"/>
                                                        </td>
                                                    </tr>
                                                </form>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <c:if test="${not empty searchNoOfPage}">
                                            <table border="1" cellpadding="5" cellspacing="5">
                                                <tr>
                                                    <c:if test="${searchCurrentPage!=1}">
                                                        <c:url var="prevLink" value="search">
                                                            <c:param name="btAction" value="SearchItem"/>
                                                            <c:param name="page" value="${searchCurrentPage-1}"/>
                                                            <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                            <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                            <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                            <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                        </c:url>
                                                        <td><a href="${prevLink}">Previous</a></td>
                                                    </c:if>
                                                    <c:forEach begin="1" end="${searchNoOfPage}" var="i">
                                                        <c:choose>
                                                            <c:when test="${searchCurrentPage eq i}">
                                                                <td>${i}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:url var="pageLink" value="search">
                                                                    <c:param name="btAction" value="SearchItem"/>
                                                                    <c:param name="page" value="${i}"/>
                                                                    <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                    <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                    <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                    <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                    <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                                </c:url>
                                                                <td><a href="${pageLink}">${i}</a></td>
                                                                </c:otherwise>    
                                                            </c:choose>
                                                        </c:forEach>
                                                        <c:if test="${searchCurrentPage < searchNoOfPage}">
                                                            <c:url var="nextLink" value="search">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${searchCurrentPage+1}"/>
                                                                <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                            </c:url>
                                                        <td><a href="${nextLink}">Next</a></td>
                                                    </c:if>            
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${empty listItems}">
                                        <h3>No record matches</h3>
                                    </c:if>
                                </div>
                            </c:when>
                            <c:when test="${ searchRentalDate eq '' and searchReturnDate  eq '' and searchAmount  eq '' and searchCar  eq '' and searchCate eq 'NULL'}">
                                <c:set var="listItems" value="${requestScope.SEARCHITEMRESULT}"/>
                                <div>
                                    <c:if test="${not empty listItems}">
                                        <table border="1">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Name</th>
                                                    <th>Color</th>
                                                    <th>Year</th>
                                                    <th>Category</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="items" items="${listItems}" varStatus="counter">
                                                <form action="addToCart" method="POST" class="register-form">
                                                    <tr>
                                                        <td>
                                                            ${counter.count}
                                                        </td>
                                                        <td>
                                                            ${items.cateModel}
                                                            <input type="hidden" name="txtCarCateID" value="${items.cateID}"/>
                                                        </td>
                                                        <td>
                                                            ${items.cateColor}
                                                        </td>
                                                        <td>
                                                            ${items.cateYear}
                                                        </td>
                                                        <td>
                                                            ${items.cateType}
                                                        </td>
                                                        <td>
                                                            ${items.catePrice}$
                                                            <input type="hidden" name="txtCarPrice" value="${items.catePrice}"/>
                                                        </td>
                                                        <td>
                                                            ${items.quantity}
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="page" value="${searchCurrentPage}"/>
                                                            <input type="hidden" name="txtSearchCar" value="${searchCar}"/>
                                                            <input type="hidden" name="cboCategory" value="${searchCate}"/>
                                                            <input type="hidden" name="txtAmount" value="${searchAmount}"/>
                                                            <input type="hidden" name="cartDateRental" value="${searchRentalDate}"/>
                                                            <input type="hidden" name="cartDateReturn" value="${searchReturnDate}"/>
                                                            <input class="btn btn-success" type="submit" value="AddToCart" name="btAction"/>
                                                        </td>
                                                    </tr>
                                                </form>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <c:if test="${not empty searchNoOfPage}">
                                            <table border="1" cellpadding="5" cellspacing="5">
                                                <tr>
                                                    <c:if test="${searchCurrentPage!=1}">
                                                        <c:url var="prevLink" value="search">
                                                            <c:param name="btAction" value="SearchItem"/>
                                                            <c:param name="page" value="${searchCurrentPage-1}"/>
                                                            <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                            <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                            <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                            <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                        </c:url>
                                                        <td><a href="${prevLink}">Previous</a></td>
                                                    </c:if>
                                                    <c:forEach begin="1" end="${searchNoOfPage}" var="i">
                                                        <c:choose>
                                                            <c:when test="${searchCurrentPage eq i}">
                                                                <td>${i}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:url var="pageLink" value="search">
                                                                    <c:param name="btAction" value="SearchItem"/>
                                                                    <c:param name="page" value="${i}"/>
                                                                    <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                    <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                    <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                    <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                    <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                                </c:url>
                                                                <td><a href="${pageLink}">${i}</a></td>
                                                                </c:otherwise>    
                                                            </c:choose>
                                                        </c:forEach>
                                                        <c:if test="${searchCurrentPage < searchNoOfPage}">
                                                            <c:url var="nextLink" value="search">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${searchCurrentPage+1}"/>
                                                                <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                            </c:url>
                                                        <td><a href="${nextLink}">Next</a></td>
                                                    </c:if>            
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${empty listItems}">
                                        <h3>No record matches</h3>
                                    </c:if>
                                </div>
                            </c:when>
                        </c:choose>
                    </c:if>
                    <c:if test="${empty searchRentalDate or empty searchReturnDate or empty searchAmount and not empty searchCar and searchCate ne 'NULL'}">
                        <c:set var="listAllItems" value="${applicationScope.ITEMLIST}"/>
                        <c:set var="listItems" value="${requestScope.SEARCHITEMRESULT}"/>
                        <c:if test="${empty listItems}">
                            <div>
                                <c:if test="${not empty listAllItems}">
                                    <table border="1">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Name</th>
                                                <th>Color</th>
                                                <th>Year</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="items" items="${listAllItems}" varStatus="counter">
                                            <form action="addToCart" method="POST" class="register-form">
                                                <tr>
                                                    <td>
                                                        ${counter.count}
                                                    </td>
                                                    <td>
                                                        ${items.cateModel}
                                                        <input type="hidden" name="txtCarCateID" value="${items.cateID}"/>
                                                    </td>
                                                    <td>
                                                        ${items.cateColor}
                                                    </td>
                                                    <td>
                                                        ${items.cateYear}
                                                    </td>
                                                    <td>
                                                        ${items.cateType}
                                                    </td>
                                                    <td>
                                                        ${items.catePrice}$
                                                        <input type="hidden" name="txtCarPrice" value="${items.catePrice}"/>
                                                    </td>
                                                    <td>
                                                        ${items.quantity}
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="page" value="${searchCurrentPage}"/>
                                                        <input type="hidden" name="txtSearchCar" value="${searchCar}"/>
                                                        <input type="hidden" name="cboCategory" value="${searchCate}"/>
                                                        <input type="hidden" name="txtAmount" value="${searchAmount}"/>
                                                        <input type="hidden" name="cartDateRental" value="${searchRentalDate}"/>
                                                        <input type="hidden" name="cartDateReturn" value="${searchReturnDate}"/>
                                                        <input class="btn btn-success" type="submit" value="AddToCart" name="btAction"/>
                                                    </td>
                                                </tr>
                                            </form>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <c:if test="${not empty searchNoOfPage}">
                                        <table border="1" cellpadding="5" cellspacing="5">
                                            <tr>
                                                <c:if test="${searchCurrentPage!=1}">
                                                    <c:url var="prevLink" value="search">
                                                        <c:param name="btAction" value="SearchItem"/>
                                                        <c:param name="page" value="${searchCurrentPage-1}"/>
                                                        <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                        <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                        <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                        <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                        <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                    </c:url>
                                                    <td><a href="${prevLink}">Previous</a></td>
                                                </c:if>
                                                <c:forEach begin="1" end="${searchNoOfPage}" var="i">
                                                    <c:choose>
                                                        <c:when test="${searchCurrentPage eq i}">
                                                            <td>${i}</td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:url var="pageLink" value="search">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${i}"/>
                                                                <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                                <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                                <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                            </c:url>
                                                            <td><a href="${pageLink}">${i}</a></td>
                                                            </c:otherwise>    
                                                        </c:choose>
                                                    </c:forEach>
                                                    <c:if test="${searchCurrentPage < searchNoOfPage}">
                                                        <c:url var="nextLink" value="search">
                                                            <c:param name="btAction" value="SearchItem"/>
                                                            <c:param name="page" value="${searchCurrentPage+1}"/>
                                                            <c:param name="txtSearchCar" value="${param.txtSearchCar}"/>
                                                            <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            <c:param name="txtDateRental" value="${param.txtDateRental}"/>
                                                            <c:param name="txtDateReturn" value="${param.txtDateReturn}"/>
                                                            <c:param name="txtAmountCar" value="${param.txtAmountCar}"/>
                                                        </c:url>
                                                    <td><a href="${nextLink}">Next</a></td>
                                                </c:if>            
                                            </tr>
                                        </table>
                                    </c:if>
                                </c:if>
                            </div>
                        </c:if>
                        <c:if test="${not empty searchCar or not empty searchCate}">
                            <h1>
                                <font color="red">
                                Please enter fully date rental, date return, amount car to search!!!!!
                                </font>
                            </h1>
                        </c:if>
                    </c:if>
                </c:if>
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="homePage">Home</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with customer account to use this function!!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
