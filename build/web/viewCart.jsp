<%-- 
    Document   : viewCart
    Created on : Feb 6, 2021, 8:58:44 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Cart Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:set var="errors" value="${requestScope.ERRORTQ}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullName}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="adminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with customer account !!!!!
                    </font>
                </h1>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullName}"/>
                <c:set var="errorROLE" value="${requestScope.errorROLE}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                </div>
                <c:set var="cartDateRental" value="${sessionScope.CARTRENTALDATE}"/>
                <c:set var="cartDateReturn" value="${sessionScope.CARTRETURNDATE}"/>
                <c:set var="ErrorUPDATEQUANTITY" value="${requestScope.UPDATER}"/>
                <c:set var="number" value="${sessionScope.NUMBERDAY}"/>
                <c:set var="errorCoupon" value="${requestScope.ERRORCHECKCOUPONS}"/>
                <c:if test="${not empty errorCoupon}">
                    <h1>
                        <font color="red">
                        ${errorCoupon}
                        </font>
                    </h1>
                </c:if>
                <c:if test="${not empty ErrorUPDATEQUANTITY}">
                    <h1>
                        <font color="red">
                        ${ErrorUPDATEQUANTITY}
                        </font>
                    </h1>
                </c:if>
                <div>
                    <div>
                        <h2>Your Cart</h2>
                    </div>
                    <c:set var="cart" value="${sessionScope.shoppingCart}"/>
                    <c:set var="CouponsDTO" value="${sessionScope.COUPONSDTOINPUT}"/>
                    <c:if test="${not empty cart}">
                        <c:set var="items" value="${cart.shoppingCart}"/>
                        <div>
                            <c:if test="${not empty items}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Car Name</th>
                                            <th>Car Type</th>
                                            <th>Price</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <form action="transfer" method="POST">
                                        <c:forEach var="item" items="${items.values()}" varStatus="counter">
                                            <tr>
                                                <td>
                                                    ${counter.count}
                                                </td>
                                                <td>
                                                    ${item.cateModel}
                                                </td>
                                                <td>
                                                    ${item.cateType}
                                                </td>
                                                <td>
                                                    ${item.catePrice}
                                                </td>
                                                <td>
                                                    <input type="text" id = "quantityNum" name="txtQuantity" value="${item.quantity}" onchange="checkNumFunction()"/>
                                                    <input type="hidden" name="txtItemID" value="${item.cateID}"/>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="chkRemove" value="${item.cateID}"/>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        <c:if test="${not empty CouponsDTO}">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Discount Value: </td>
                                                <td>${CouponsDTO.discountAmount}%</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <button type="button" onclick="goToForCustomerPage()">Continue Shopping</button>
                                                </td>
                                                <td>
                                                    Total: ${(cart.total * number) - (((cart.total * number) * CouponsDTO.discountAmount)/100)}$
                                                </td>
                                                <td>
                                                    <input type="submit" name="btAction" value="UpdateFromCart"/>
                                                </td>
                                                <td>
                                                    <input type="submit" name="btAction" value="RemoveFromCart" onclick="return ConfirmFunction()"/>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${empty CouponsDTO}">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <button type="button" onclick="goToForCustomerPage()">Continue Shopping</button>
                                                </td>
                                                <td>
                                                    Total: ${cart.total * number}$
                                                </td>
                                                <td>
                                                    <input type="submit" name="btAction" value="UpdateFromCart"/>
                                                </td>
                                                <td>
                                                    <input type="submit" name="btAction" value="RemoveFromCart" onclick="return ConfirmFunction()"/>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </form>
                                    </tbody>
                                </table><br/>
                                <c:if test="${not empty errors.quantityErr}">
                                    <font color="red">
                                    ${errors.quantityErr}
                                    </font><br/>
                                </c:if>
                                <form action="forCheckOut" method="POST">
                                    Date Rental: <input type="text" name="txtDateWantRental" value="${cartDateRental}"/><br/>
                                    Date Return: <input type="text" name="txtDateWantReturn" value="${cartDateReturn}"/><br/>
                                    Coupon Code: <input type="text" name="txtCouponCode" value="${param.txtCouponCode}"/>
                                    <input type="submit" value="Apply" name="btAction"/><br/>
                                    <input type="hidden" name="txtTotalPrice" value="${cart.total * number}"/>
                                    <input type="submit" value="Check Out" name="btAction"/>
                                </form>
                            </c:if>
                        </div>
                        <c:if test="${empty items}">
                            <h3>Item not exist!!!</h3>
                        </c:if>
                    </c:if>
                </div>    
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Home</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with customer account to use this function!!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
