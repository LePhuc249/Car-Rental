<%-- 
    Document   : createNewAccountError
    Created on : Feb 7, 2021, 8:58:20 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Account Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <div>
            <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                <div class="container-fluid">
                    <a class="navbar-branch" href="#">
                        <img src= "img/Logo.png" height = "100" alt="#">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </a>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="homePage">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="loginPage">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-3 col-sm-6 col-xs-12 row-container">
                    <h2>Create New Account</h2>
                    <c:set var="errors" value="${requestScope.INSERTERR}"/>
                    <form action = "createAccount" method="post" class="register-form">
                        <div class="form-group">
                            Email <input type="text" name="txtEmail" value="${param.txtEmail}"/>(2-50 character)<br/>
                            <c:if test="${not empty errors.emailLengthError}">
                                <font color="red">
                                ${errors.emailLengthError}
                                </font><br/>
                            </c:if>
                            <c:if test="${not empty errors.emailTypeErr}">
                                <font color="red">
                                ${errors.emailTypeErr}
                                </font><br/>
                            </c:if>
                            <c:if test="${not empty errors.emailIsExist}">
                                <font color="red">
                                ${errors.emailIsExist}
                                </font><br/>
                            </c:if>
                        </div>
                        <div class="form-group">
                            Password* <input type="password" name="txtPassword" value=""/>(6-50 character)<br/>
                            <c:if test="${not empty errors.passwordLengthError}">
                                <font color="red">
                                ${errors.passwordLengthError}
                                </font><br/>
                            </c:if>
                        </div>
                        <div class="form-group">
                            Confirm <input type="password" name="txtConfirm" value=""/><br/>
                            <c:if test="${not empty errors.confirmNotMatch}">
                                <font color="red">
                                ${errors.confirmNotMatch}
                                </font><br/>
                            </c:if>
                        </div>
                        <div class="form-group">
                            Full name* <input type="text" name="txtFullname" value="${param.txtFullname}"/>(2-50 character)<br/>
                            <c:if test="${not empty errors.fullnameLengthError}">
                                <font color="red">
                                ${errors.fullnameLengthError}
                                </font><br/>
                            </c:if>
                        </div>
                        <div class="form-group">
                            Phone <input type="text" name="txtPhone" value="${param.txtPhone}"/>(2-20 character)<br/>
                            <c:if test="${not empty errors.phoneLengthErr}">
                                <font color="red">
                                ${errors.phoneLengthErr}
                                </font><br/>
                            </c:if>
                        </div>
                        <div class="form-group">
                            Address <input type="text" name="txtAddress" value="${param.txtAddress}"/>(2-200 character)<br/>
                            <c:if test="${not empty errors.addressLengthErr}">
                                <font color="red">
                                ${errors.addressLengthErr}
                                </font><br/>
                            </c:if>
                        </div>
                        <input class="btn btn-success btn-block my-3" type="submit" value="Create New Account" name="btAction"/>
                        <input class="btn btn-reset btn-block my-3" type="reset" value="Reset"/>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
