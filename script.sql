USE [CarRental]
GO
ALTER TABLE [dbo].[UserOrderDetail] DROP CONSTRAINT [FK__UserOrder__order__3B75D760]
GO
ALTER TABLE [dbo].[UserOrderDetail] DROP CONSTRAINT [FK__UserOrder__carID__3C69FB99]
GO
ALTER TABLE [dbo].[UserOrder] DROP CONSTRAINT [FK__UserOrder__statu__36B12243]
GO
ALTER TABLE [dbo].[UserOrder] DROP CONSTRAINT [FK__UserOrder__custo__34C8D9D1]
GO
ALTER TABLE [dbo].[UserOrder] DROP CONSTRAINT [FK__UserOrder__coupo__38996AB5]
GO
ALTER TABLE [dbo].[Feedback] DROP CONSTRAINT [FK__Feedback__orderL__412EB0B6]
GO
ALTER TABLE [dbo].[Feedback] DROP CONSTRAINT [FK__Feedback__accoun__403A8C7D]
GO
ALTER TABLE [dbo].[CarCategory] DROP CONSTRAINT [FK__CarCatego__cateC__1FCDBCEB]
GO
ALTER TABLE [dbo].[Car] DROP CONSTRAINT [FK__Car__status__286302EC]
GO
ALTER TABLE [dbo].[Car] DROP CONSTRAINT [FK__Car__cateCarID__25869641]
GO
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK__Account__roleID__145C0A3F]
GO
ALTER TABLE [dbo].[Feedback] DROP CONSTRAINT [DF__Feedback__feedba__4222D4EF]
GO
/****** Object:  Index [UQ__Coupons__1D19F4DAD67AA57D]    Script Date: 3/18/2021 10:10:35 PM ******/
ALTER TABLE [dbo].[Coupons] DROP CONSTRAINT [UQ__Coupons__1D19F4DAD67AA57D]
GO
/****** Object:  Index [UQ__Car__C5DF234C28E179A5]    Script Date: 3/18/2021 10:10:35 PM ******/
ALTER TABLE [dbo].[Car] DROP CONSTRAINT [UQ__Car__C5DF234C28E179A5]
GO
/****** Object:  Table [dbo].[UserOrderStatus]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[UserOrderStatus]
GO
/****** Object:  Table [dbo].[UserOrderDetail]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[UserOrderDetail]
GO
/****** Object:  Table [dbo].[UserOrder]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[UserOrder]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[Role]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[Feedback]
GO
/****** Object:  Table [dbo].[Coupons]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[Coupons]
GO
/****** Object:  Table [dbo].[CarType]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[CarType]
GO
/****** Object:  Table [dbo].[CarStatus]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[CarStatus]
GO
/****** Object:  Table [dbo].[CarCategory]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[CarCategory]
GO
/****** Object:  Table [dbo].[Car]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[Car]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP TABLE [dbo].[Account]
GO
USE [master]
GO
/****** Object:  Database [CarRental]    Script Date: 3/18/2021 10:10:35 PM ******/
DROP DATABASE [CarRental]
GO
/****** Object:  Database [CarRental]    Script Date: 3/18/2021 10:10:35 PM ******/
CREATE DATABASE [CarRental]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CarRental', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\CarRental.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CarRental_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\CarRental_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CarRental] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CarRental].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CarRental] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CarRental] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CarRental] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CarRental] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CarRental] SET ARITHABORT OFF 
GO
ALTER DATABASE [CarRental] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CarRental] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CarRental] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CarRental] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CarRental] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CarRental] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CarRental] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CarRental] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CarRental] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CarRental] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CarRental] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CarRental] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CarRental] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CarRental] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CarRental] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CarRental] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CarRental] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CarRental] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CarRental] SET  MULTI_USER 
GO
ALTER DATABASE [CarRental] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CarRental] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CarRental] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CarRental] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CarRental] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CarRental]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[emailAccount] [varchar](50) NOT NULL,
	[passwordAccount] [varchar](100) NOT NULL,
	[fullnameAccount] [varchar](50) NULL,
	[phoneAccount] [varchar](20) NULL,
	[addressAccount] [varchar](500) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[statusAccount] [varchar](10) NULL DEFAULT ('New'),
	[codeActive] [varchar](10) NULL,
	[roleID] [varchar](10) NULL DEFAULT ('ST'),
PRIMARY KEY CLUSTERED 
(
	[emailAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Car]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Car](
	[carID] [varchar](20) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[cateCarID] [varchar](20) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[status] [varchar](10) NULL DEFAULT ('Fe'),
	[isDeleted] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[carID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarCategory]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarCategory](
	[cateCarID] [varchar](20) NOT NULL,
	[cateCarModel] [varchar](50) NULL,
	[cateCarMake] [varchar](50) NULL,
	[cateCarColor] [varchar](20) NULL,
	[cateCarYear] [varchar](5) NULL,
	[cateCarPrice] [float] NULL,
	[cateCarType] [varchar](10) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[isDeleted] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[cateCarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarStatus]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarStatus](
	[carStatusID] [varchar](10) NOT NULL,
	[carStatusContent] [varchar](50) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[isDeleted] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[carStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarType]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarType](
	[carTypeID] [varchar](10) NOT NULL,
	[carTypeName] [varchar](50) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[isDeleted] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[carTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coupons]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupons](
	[couponID] [varchar](30) NOT NULL,
	[couponCode] [varchar](50) NOT NULL,
	[discount_amount] [int] NULL,
	[expiration_date] [varchar](15) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[isDeleted] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[couponID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feedback](
	[FeedbackID] [varchar](20) NOT NULL,
	[accountID] [varchar](50) NULL,
	[orderLineID] [varchar](30) NULL,
	[feedbackTime] [datetime] NULL,
	[feedbackContent] [varchar](100) NULL,
	[counter] [int] NULL,
	[status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[FeedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[roleID] [varchar](10) NOT NULL,
	[roleName] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserOrder]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserOrder](
	[orderID] [varchar](20) NOT NULL,
	[customerID] [varchar](50) NULL,
	[rentalDate] [varchar](20) NOT NULL,
	[returnDate] [varchar](20) NOT NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[status] [varchar](20) NULL DEFAULT ('Pr'),
	[totalPrice] [float] NULL,
	[couponID] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[orderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserOrderDetail]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserOrderDetail](
	[orderLineID] [varchar](30) NOT NULL,
	[orderID] [varchar](20) NULL,
	[carID] [varchar](20) NULL,
	[carProperty] [varchar](200) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[orderLineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserOrderStatus]    Script Date: 3/18/2021 10:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserOrderStatus](
	[orderStatusID] [varchar](20) NOT NULL,
	[orderStatusName] [varchar](50) NULL,
	[createDate] [datetime] NULL DEFAULT (getdate()),
	[isDeleted] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[orderStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'admin@gmail.com', N'123456', N'Admin', N'1111111117', N'None', CAST(N'2021-02-25 08:23:35.427' AS DateTime), N'Active', NULL, N'AD')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'bachnv@gmail.com', N'123456', N'Nguyen Viet Bach', N'1111111113', N'Binh Thanh', CAST(N'2021-02-25 08:02:33.503' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'huylng@gmail.com', N'123456', N'Le Nguyen Gia Huy', N'1111111114', N'Binh Duong', CAST(N'2021-02-25 08:02:59.993' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'ledachoangphuc249@gmail.com', N'123456', N'Le Phuc', N'0772008835', N'Tan phu', CAST(N'2021-03-05 23:44:26.247' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'linhvtt@gmail.com', N'123456', N'Vu Thi Thuy Linh', N'1111111115', N'Binh Phuoc', CAST(N'2021-02-25 08:12:41.217' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'phuchgt@gmail.com', N'123456', N'Hoang Gia Thien Phuc', N'1111111112', N'Buon Me Thuoc', CAST(N'2021-02-25 08:02:08.283' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'phucldh@gmail.com', N'123456', N'Le Dac Hoang Phuc', N'1111111111', N'Tan Phu', CAST(N'2021-02-25 08:01:24.460' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Account] ([emailAccount], [passwordAccount], [fullnameAccount], [phoneAccount], [addressAccount], [createDate], [statusAccount], [codeActive], [roleID]) VALUES (N'trucntt@gmail.com', N'123456', N'Nguyen Thi Thanh Truc', N'1111111116', N'Quan 9', CAST(N'2021-02-25 08:12:54.270' AS DateTime), N'Active', NULL, N'CT')
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_1', N'502208', N'CC_1', CAST(N'2021-02-26 08:22:56.180' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_10', N'549631', N'CC_2', CAST(N'2021-02-26 08:29:04.103' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_100', N'504710', N'CC_32', CAST(N'2021-03-03 10:55:27.377' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_101', N'888928', N'CC_33', CAST(N'2021-03-03 10:55:41.307' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_102', N'561162', N'CC_33', CAST(N'2021-03-03 10:55:56.333' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_103', N'624865', N'CC_33', CAST(N'2021-03-03 10:56:10.257' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_104', N'526925', N'CC_34', CAST(N'2021-03-03 10:56:26.047' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_105', N'773746', N'CC_34', CAST(N'2021-03-03 10:56:43.907' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_106', N'078237', N'CC_35', CAST(N'2021-03-03 10:56:58.550' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_107', N'737254', N'CC_35', CAST(N'2021-03-03 10:57:12.673' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_108', N'906582', N'CC_35', CAST(N'2021-03-03 10:57:31.030' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_109', N'292640', N'CC_35', CAST(N'2021-03-03 10:57:43.260' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_11', N'715884', N'CC_3', CAST(N'2021-02-26 08:29:15.233' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_110', N'476426', N'CC_35', CAST(N'2021-03-03 10:57:55.317' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_12', N'157859', N'CC_3', CAST(N'2021-02-26 08:29:25.943' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_13', N'655868', N'CC_3', CAST(N'2021-02-26 08:31:17.107' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_14', N'834521', N'CC_3', CAST(N'2021-02-26 08:31:58.697' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_15', N'517893', N'CC_3', CAST(N'2021-02-26 08:32:16.697' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_16', N'698356', N'CC_5', CAST(N'2021-03-03 09:26:58.207' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_17', N'893134', N'CC_5', CAST(N'2021-03-03 09:27:13.097' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_18', N'365308', N'CC_5', CAST(N'2021-03-03 09:27:26.997' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_19', N'485722', N'CC_4', CAST(N'2021-03-03 09:27:46.140' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_2', N'089848', N'CC_1', CAST(N'2021-02-26 08:25:54.680' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_20', N'259879', N'CC_4', CAST(N'2021-03-03 09:27:59.187' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_21', N'248018', N'CC_6', CAST(N'2021-03-03 09:28:56.300' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_22', N'187012', N'CC_6', CAST(N'2021-03-03 09:29:09.607' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_23', N'161109', N'CC_6', CAST(N'2021-03-03 09:29:21.510' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_24', N'497344', N'CC_7', CAST(N'2021-03-03 09:29:37.250' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_25', N'404398', N'CC_7', CAST(N'2021-03-03 09:29:49.420' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_26', N'558996', N'CC_7', CAST(N'2021-03-03 09:30:06.960' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_27', N'555028', N'CC_8', CAST(N'2021-03-03 09:30:19.737' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_28', N'804875', N'CC_8', CAST(N'2021-03-03 09:31:00.283' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_29', N'290310', N'CC_8', CAST(N'2021-03-03 09:32:37.120' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_3', N'205598', N'CC_1', CAST(N'2021-02-26 08:26:35.900' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_30', N'783842', N'CC_8', CAST(N'2021-03-03 09:32:47.773' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_31', N'899432', N'CC_9', CAST(N'2021-03-03 09:32:57.907' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_32', N'688610', N'CC_9', CAST(N'2021-03-03 09:33:21.103' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_33', N'566329', N'CC_10', CAST(N'2021-03-03 09:33:37.543' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_34', N'302664', N'CC_11', CAST(N'2021-03-03 10:10:45.363' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_35', N'561705', N'CC_11', CAST(N'2021-03-03 10:10:58.453' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_36', N'271394', N'CC_11', CAST(N'2021-03-03 10:11:14.463' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_37', N'866594', N'CC_12', CAST(N'2021-03-03 10:12:53.150' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_38', N'622573', N'CC_12', CAST(N'2021-03-03 10:13:14.907' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_39', N'132576', N'CC_12', CAST(N'2021-03-03 10:13:28.823' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_4', N'930865', N'CC_1', CAST(N'2021-02-26 08:27:38.540' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_40', N'427814', N'CC_12', CAST(N'2021-03-03 10:13:43.397' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_41', N'670026', N'CC_13', CAST(N'2021-03-03 10:13:58.850' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_42', N'950608', N'CC_13', CAST(N'2021-03-03 10:14:16.097' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_43', N'541912', N'CC_14', CAST(N'2021-03-03 10:14:41.073' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_44', N'082513', N'CC_14', CAST(N'2021-03-03 10:14:56.833' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_45', N'418643', N'CC_14', CAST(N'2021-03-03 10:15:11.573' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_46', N'000003', N'CC_15', CAST(N'2021-03-03 10:34:01.897' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_47', N'428703', N'CC_15', CAST(N'2021-03-03 10:34:23.460' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_48', N'242196', N'CC_15', CAST(N'2021-03-03 10:34:40.907' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_49', N'168270', N'CC_16', CAST(N'2021-03-03 10:37:05.747' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_5', N'063005', N'CC_1', CAST(N'2021-02-26 08:27:53.760' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_50', N'793229', N'CC_16', CAST(N'2021-03-03 10:37:24.640' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_51', N'317592', N'CC_17', CAST(N'2021-03-03 10:37:38.657' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_52', N'862542', N'CC_18', CAST(N'2021-03-03 10:37:52.950' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_53', N'063559', N'CC_18', CAST(N'2021-03-03 10:38:04.170' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_54', N'394979', N'CC_19', CAST(N'2021-03-03 10:38:22.330' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_55', N'074315', N'CC_19', CAST(N'2021-03-03 10:41:27.363' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_56', N'363379', N'CC_20', CAST(N'2021-03-03 10:41:41.393' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_57', N'999104', N'CC_20', CAST(N'2021-03-03 10:41:57.553' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_58', N'543034', N'CC_20', CAST(N'2021-03-03 10:42:10.543' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_59', N'521410', N'CC_20', CAST(N'2021-03-03 10:42:24.340' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_6', N'105419', N'CC_2', CAST(N'2021-02-26 08:28:08.323' AS DateTime), N'Bu', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_60', N'841947', N'CC_20', CAST(N'2021-03-03 10:42:38.650' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_61', N'700212', N'CC_21', CAST(N'2021-03-03 10:44:31.150' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_62', N'563282', N'CC_21', CAST(N'2021-03-03 10:44:42.247' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_63', N'939882', N'CC_22', CAST(N'2021-03-03 10:44:54.783' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_64', N'753722', N'CC_22', CAST(N'2021-03-03 10:45:07.057' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_65', N'169113', N'CC_22', CAST(N'2021-03-03 10:45:25.513' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_66', N'943423', N'CC_22', CAST(N'2021-03-03 10:45:38.510' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_67', N'917692', N'CC_23', CAST(N'2021-03-03 10:45:52.733' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_68', N'061540', N'CC_23', CAST(N'2021-03-03 10:46:05.377' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_69', N'164867', N'CC_23', CAST(N'2021-03-03 10:46:18.480' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_7', N'290398', N'CC_2', CAST(N'2021-02-26 08:28:20.440' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_70', N'406040', N'CC_23', CAST(N'2021-03-03 10:46:35.660' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_71', N'722620', N'CC_24', CAST(N'2021-03-03 10:48:09.723' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_72', N'625466', N'CC_24', CAST(N'2021-03-03 10:48:21.510' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_73', N'363108', N'CC_24', CAST(N'2021-03-03 10:48:32.207' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_74', N'659147', N'CC_25', CAST(N'2021-03-03 10:48:45.203' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_75', N'784189', N'CC_25', CAST(N'2021-03-03 10:48:56.520' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_76', N'344380', N'CC_25', CAST(N'2021-03-03 10:49:40.530' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_77', N'829114', N'CC_26', CAST(N'2021-03-03 10:49:53.847' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_78', N'539545', N'CC_26', CAST(N'2021-03-03 10:50:06.820' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_79', N'633420', N'CC_26', CAST(N'2021-03-03 10:50:19.347' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_8', N'307813', N'CC_2', CAST(N'2021-02-26 08:28:36.253' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_80', N'882393', N'CC_26', CAST(N'2021-03-03 10:50:29.940' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_81', N'068856', N'CC_27', CAST(N'2021-03-03 10:50:44.853' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_82', N'364090', N'CC_27', CAST(N'2021-03-03 10:50:57.637' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_83', N'164176', N'CC_27', CAST(N'2021-03-03 10:51:11.640' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_84', N'795347', N'CC_27', CAST(N'2021-03-03 10:51:24.830' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_85', N'792050', N'CC_27', CAST(N'2021-03-03 10:51:40.187' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_86', N'690540', N'CC_27', CAST(N'2021-03-03 10:51:51.603' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_87', N'270729', N'CC_28', CAST(N'2021-03-03 10:52:05.433' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_88', N'038392', N'CC_28', CAST(N'2021-03-03 10:52:23.787' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_89', N'731675', N'CC_28', CAST(N'2021-03-03 10:52:37.580' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_9', N'264117', N'CC_2', CAST(N'2021-02-26 08:28:53.397' AS DateTime), N'Fe', 0)
GO
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_90', N'003002', N'CC_28', CAST(N'2021-03-03 10:53:07.353' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_91', N'166017', N'CC_29', CAST(N'2021-03-03 10:53:35.600' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_92', N'710890', N'CC_29', CAST(N'2021-03-03 10:53:46.923' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_93', N'912990', N'CC_30', CAST(N'2021-03-03 10:53:58.513' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_94', N'621615', N'CC_31', CAST(N'2021-03-03 10:54:08.800' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_95', N'769040', N'CC_31', CAST(N'2021-03-03 10:54:21.777' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_96', N'933127', N'CC_32', CAST(N'2021-03-03 10:54:36.570' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_97', N'538725', N'CC_32', CAST(N'2021-03-03 10:54:48.833' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_98', N'002419', N'CC_32', CAST(N'2021-03-03 10:55:02.923' AS DateTime), N'Fe', 0)
INSERT [dbo].[Car] ([carID], [VIN], [cateCarID], [createDate], [status], [isDeleted]) VALUES (N'C_99', N'890160', N'CC_32', CAST(N'2021-03-03 10:55:13.803' AS DateTime), N'Fe', 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_1', N'Kia Cerato', N'KIA', N'Red', N'2021', 100, N'SD', CAST(N'2021-02-25 08:54:08.680' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_10', N'Toyota Vios', N'Toyota', N'Black', N'2021', 101, N'SD', CAST(N'2021-02-25 09:04:33.357' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_11', N'Chevrolet Colorado', N'Chevrolet ', N'Black', N'2021', 150, N'PT', CAST(N'2021-03-03 09:37:56.923' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_12', N'Ford F-350', N'Ford', N'White', N'2021', 170, N'PT', CAST(N'2021-03-03 09:38:35.540' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_13', N'Ford F-450', N'Ford', N'Blue', N'2021', 175, N'PT', CAST(N'2021-03-03 09:42:13.460' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_14', N'RAM 3500', N'RAM', N'Black', N'2021', 155, N'PT', CAST(N'2021-03-03 09:42:44.910' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_15', N'Jaguar F-TYPE', N'Jaguar', N'Black', N'2021', 125, N'CP', CAST(N'2021-03-03 09:44:12.587' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_16', N' Audi RS 5', N'Audi', N'Blue', N'2021', 120, N'CP', CAST(N'2021-03-03 09:44:33.603' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_17', N'BMW 430', N'BMW', N'Black', N'2021', 130, N'CP', CAST(N'2021-03-03 09:45:01.293' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_18', N'BMW M4', N'BMW', N'Black', N'2021', 135, N'CP', CAST(N'2021-03-03 09:45:25.367' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_19', N'Lexus RC 350', N'Lexus', N'White', N'2021', 138, N'CP', CAST(N'2021-03-03 09:45:45.103' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_2', N'Hyundai Accent', N'Hyundai', N'Black', N'2021', 102, N'HB', CAST(N'2021-02-25 08:56:48.267' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_20', N'Aston Martin DB11', N'Aston', N'White', N'2021', 200, N'SC', CAST(N'2021-03-03 09:48:14.253' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_21', N'Aston Martin DBS', N'Aston', N'Black', N'2021', 205, N'SC', CAST(N'2021-03-03 09:48:25.970' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_22', N' Aston Martin Vantage', N'Aston', N'Black', N'2021', 195, N'SC', CAST(N'2021-03-03 09:48:58.007' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_23', N'Audi RS 5', N'Audi', N'White', N'2021', 180, N'SC', CAST(N'2021-03-03 09:49:24.130' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_24', N'Audi S4', N'Audi', N'Black', N'2021', 186, N'SC', CAST(N'2021-03-03 09:52:22.887' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_25', N'Audi S5', N'Audi', N'Blue', N'2021', 189, N'SC', CAST(N'2021-03-03 09:52:48.500' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_26', N'Chrysler Pacifica Hybrid', N'Chrysler', N'Black', N'2021', 250, N'MV', CAST(N'2021-03-03 09:54:16.810' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_27', N'Ford Transit-350', N'Ford', N'Black', N'2021', 255, N'MV', CAST(N'2021-03-03 09:54:38.550' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_28', N'Mercedes-Benz Sprinter 2500', N'Mercedes-Benz', N'Black', N'2021', 260, N'MV', CAST(N'2021-03-03 09:55:02.287' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_29', N'Mercedes-Benz Sprinter 3500', N'Mercedes-Benz', N'Black', N'2021', 265, N'MV', CAST(N'2021-03-03 09:57:03.960' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_3', N'Kia Cerato', N'KIA', N'Blue', N'2020', 103, N'SD', CAST(N'2021-02-25 08:56:54.530' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_30', N'RAM ProMaster 3500', N'RAM', N'White', N'2021', 245, N'MV', CAST(N'2021-03-03 09:58:04.883' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_31', N'Subaru Outback', N'Subaru', N'Black', N'2021', 90, N'SW', CAST(N'2021-03-03 10:04:58.947' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_32', N'Mercedes-Benz E-Class Wagon', N'Mercedes-Benz', N'Black', N'2021', 95, N'SW', CAST(N'2021-03-03 10:05:13.147' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_33', N'BMW Z4', N'BMW', N'Red', N'2021', 320, N'CV', CAST(N'2021-03-03 10:07:38.300' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_34', N'Mercedes-AMG SLC43', N'Mercedes-Benz', N'White', N'2021', 300, N'CV', CAST(N'2021-03-03 10:07:49.140' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_35', N'Rolls-Royce Dawn', N'Rolls-Royce', N'Black', N'2021', 500, N'CV', CAST(N'2021-03-03 10:08:25.923' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_4', N'Kia Cerato', N'KIA', N'Black', N'2021', 104, N'SD', CAST(N'2021-02-25 08:59:49.540' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_5', N'Hyundai Accent', N'Hyundai', N'Blue', N'2021', 105, N'HB', CAST(N'2021-02-25 09:00:22.353' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_6', N'Honda City', N'Honda', N'Red', N'2020', 106, N'SD', CAST(N'2021-02-25 09:02:49.803' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_7', N'Honda City', N'Honda', N'Blue', N'2020', 107, N'SD', CAST(N'2021-02-25 09:03:33.440' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_8', N'Honda City', N'Honda', N'Black', N'2020', 108, N'SD', CAST(N'2021-02-25 09:03:47.090' AS DateTime), 0)
INSERT [dbo].[CarCategory] ([cateCarID], [cateCarModel], [cateCarMake], [cateCarColor], [cateCarYear], [cateCarPrice], [cateCarType], [createDate], [isDeleted]) VALUES (N'CC_9', N'Toyota Vios', N'Toyota', N'Red', N'2021', 109, N'SD', CAST(N'2021-02-25 09:04:16.677' AS DateTime), 0)
INSERT [dbo].[CarStatus] ([carStatusID], [carStatusContent], [createDate], [isDeleted]) VALUES (N'Bu', N'Busy', CAST(N'2021-02-26 08:22:16.643' AS DateTime), 0)
INSERT [dbo].[CarStatus] ([carStatusID], [carStatusContent], [createDate], [isDeleted]) VALUES (N'Fe', N'Free', CAST(N'2021-02-26 08:22:09.083' AS DateTime), 0)
INSERT [dbo].[CarStatus] ([carStatusID], [carStatusContent], [createDate], [isDeleted]) VALUES (N'Ma', N'Maintenance', CAST(N'2021-02-26 08:22:52.107' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'CP', N'COUPE', CAST(N'2021-02-25 08:42:24.670' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'CV', N'CONVERTIBLE', CAST(N'2021-02-25 08:44:37.267' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'HB', N'HATCHBACK', CAST(N'2021-02-25 08:44:24.680' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'MV', N'MINIVAN', CAST(N'2021-02-25 08:44:48.323' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'PT', N'PICKUP TRUCK', CAST(N'2021-02-25 08:44:58.613' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'SC', N'SPORTS CAR', CAST(N'2021-02-25 08:43:12.570' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'SD', N'SEDAN', CAST(N'2021-02-25 08:40:44.037' AS DateTime), 0)
INSERT [dbo].[CarType] ([carTypeID], [carTypeName], [createDate], [isDeleted]) VALUES (N'SW', N'STATION WAGON', CAST(N'2021-02-25 08:44:14.393' AS DateTime), 0)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_1', N'ABC', 10, N'2021-05-01', CAST(N'2021-02-27 22:29:42.800' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_10', N'HAC', 5, N'2021-05-01', CAST(N'2021-02-27 22:31:43.400' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_2', N'DEF', 15, N'2021-05-01', CAST(N'2021-02-27 22:29:59.530' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_3', N'GHI', 20, N'2021-05-01', CAST(N'2021-02-27 22:30:10.807' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_4', N'JKL', 25, N'2021-05-01', CAST(N'2021-02-27 22:30:22.437' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_5', N'POI', 30, N'2021-05-01', CAST(N'2021-02-27 22:30:31.453' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_6', N'A00', 35, N'2021-05-01', CAST(N'2021-02-27 22:30:49.260' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_7', N'XYZ', 40, N'2021-05-01', CAST(N'2021-02-27 22:31:01.470' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_8', N'AZC', 45, N'2021-05-01', CAST(N'2021-02-27 22:31:11.120' AS DateTime), 1)
INSERT [dbo].[Coupons] ([couponID], [couponCode], [discount_amount], [expiration_date], [createDate], [isDeleted]) VALUES (N'CP_9', N'MON', 50, N'2021-05-01', CAST(N'2021-02-27 22:31:27.000' AS DateTime), 1)
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (N'AD', N'Admin')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (N'CT', N'Customer')
INSERT [dbo].[UserOrder] ([orderID], [customerID], [rentalDate], [returnDate], [createDate], [status], [totalPrice], [couponID]) VALUES (N'O_1', N'phucldh@gmail.com', N'2021-03-07', N'2021-03-08', CAST(N'2021-03-06 22:12:49.540' AS DateTime), N'Co', 364.5, N'CP_1')
INSERT [dbo].[UserOrder] ([orderID], [customerID], [rentalDate], [returnDate], [createDate], [status], [totalPrice], [couponID]) VALUES (N'O_2', N'phucldh@gmail.com', N'2021-03-07', N'2021-03-08', CAST(N'2021-03-06 22:13:18.620' AS DateTime), N'Co', 255, N'CP_9')
INSERT [dbo].[UserOrder] ([orderID], [customerID], [rentalDate], [returnDate], [createDate], [status], [totalPrice], [couponID]) VALUES (N'O_3', N'phucldh@gmail.com', N'2021-03-07', N'2021-03-08', CAST(N'2021-03-06 22:55:30.110' AS DateTime), N'Co', 130, NULL)
INSERT [dbo].[UserOrder] ([orderID], [customerID], [rentalDate], [returnDate], [createDate], [status], [totalPrice], [couponID]) VALUES (N'O_4', N'phucldh@gmail.com', N'2021-03-07', N'2021-03-08', CAST(N'2021-03-06 23:13:24.100' AS DateTime), N'Pr', 170, NULL)
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_1', N'O_1', N'C_11', N'Kia Cerato_Blue_2020_KIA', CAST(N'2021-03-06 22:12:49.560' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_10', N'O_3', N'C_51', N'BMW 430_Black_2021_BMW', CAST(N'2021-03-06 22:55:30.117' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_11', N'O_4', N'C_37', N'Ford F-350_White_2021_Ford', CAST(N'2021-03-06 23:13:24.110' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_2', N'O_1', N'C_10', N'Hyundai Accent_Black_2021_Hyundai', CAST(N'2021-03-06 22:12:49.563' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_3', N'O_1', N'C_1', N'Kia Cerato_Red_2021_KIA', CAST(N'2021-03-06 22:12:49.567' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_4', N'O_1', N'C_2', N'Kia Cerato_Red_2021_KIA', CAST(N'2021-03-06 22:12:49.567' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_5', N'O_2', N'C_16', N'Hyundai Accent_Blue_2021_Hyundai', CAST(N'2021-03-06 22:13:18.630' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_6', N'O_2', N'C_12', N'Kia Cerato_Blue_2020_KIA', CAST(N'2021-03-06 22:13:18.630' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_7', N'O_2', N'C_6', N'Hyundai Accent_Black_2021_Hyundai', CAST(N'2021-03-06 22:13:18.633' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_8', N'O_2', N'C_3', N'Kia Cerato_Red_2021_KIA', CAST(N'2021-03-06 22:13:18.633' AS DateTime))
INSERT [dbo].[UserOrderDetail] ([orderLineID], [orderID], [carID], [carProperty], [createDate]) VALUES (N'OL_9', N'O_2', N'C_4', N'Kia Cerato_Red_2021_KIA', CAST(N'2021-03-06 22:13:18.637' AS DateTime))
INSERT [dbo].[UserOrderStatus] ([orderStatusID], [orderStatusName], [createDate], [isDeleted]) VALUES (N'Ca', N'Cancel', CAST(N'2021-02-26 09:30:34.293' AS DateTime), 0)
INSERT [dbo].[UserOrderStatus] ([orderStatusID], [orderStatusName], [createDate], [isDeleted]) VALUES (N'Co', N'Confirm', CAST(N'2021-02-26 09:30:17.180' AS DateTime), 0)
INSERT [dbo].[UserOrderStatus] ([orderStatusID], [orderStatusName], [createDate], [isDeleted]) VALUES (N'Fi', N'Finish', CAST(N'2021-02-26 09:30:30.447' AS DateTime), 0)
INSERT [dbo].[UserOrderStatus] ([orderStatusID], [orderStatusName], [createDate], [isDeleted]) VALUES (N'Hi', N'Hiring', CAST(N'2021-02-26 09:30:24.783' AS DateTime), 0)
INSERT [dbo].[UserOrderStatus] ([orderStatusID], [orderStatusName], [createDate], [isDeleted]) VALUES (N'Pr', N'Processing', CAST(N'2021-02-26 09:30:08.813' AS DateTime), 0)
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Car__C5DF234C28E179A5]    Script Date: 3/18/2021 10:10:35 PM ******/
ALTER TABLE [dbo].[Car] ADD UNIQUE NONCLUSTERED 
(
	[VIN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Coupons__1D19F4DAD67AA57D]    Script Date: 3/18/2021 10:10:35 PM ******/
ALTER TABLE [dbo].[Coupons] ADD UNIQUE NONCLUSTERED 
(
	[couponCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [feedbackTime]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([roleID])
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD FOREIGN KEY([cateCarID])
REFERENCES [dbo].[CarCategory] ([cateCarID])
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD FOREIGN KEY([status])
REFERENCES [dbo].[CarStatus] ([carStatusID])
GO
ALTER TABLE [dbo].[CarCategory]  WITH CHECK ADD FOREIGN KEY([cateCarType])
REFERENCES [dbo].[CarType] ([carTypeID])
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD FOREIGN KEY([accountID])
REFERENCES [dbo].[Account] ([emailAccount])
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD FOREIGN KEY([orderLineID])
REFERENCES [dbo].[UserOrderDetail] ([orderLineID])
GO
ALTER TABLE [dbo].[UserOrder]  WITH CHECK ADD FOREIGN KEY([couponID])
REFERENCES [dbo].[Coupons] ([couponID])
GO
ALTER TABLE [dbo].[UserOrder]  WITH CHECK ADD FOREIGN KEY([customerID])
REFERENCES [dbo].[Account] ([emailAccount])
GO
ALTER TABLE [dbo].[UserOrder]  WITH CHECK ADD FOREIGN KEY([status])
REFERENCES [dbo].[UserOrderStatus] ([orderStatusID])
GO
ALTER TABLE [dbo].[UserOrderDetail]  WITH CHECK ADD FOREIGN KEY([carID])
REFERENCES [dbo].[Car] ([carID])
GO
ALTER TABLE [dbo].[UserOrderDetail]  WITH CHECK ADD FOREIGN KEY([orderID])
REFERENCES [dbo].[UserOrder] ([orderID])
GO
USE [master]
GO
ALTER DATABASE [CarRental] SET  READ_WRITE 
GO
