/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Account;

/**
 *
 * @author Admin
 */
public class AccountCreateError {

    private String emailLengthError;
    private String emailTypeErr;
    private String emailIsExist;
    private String passwordLengthError;
    private String confirmNotMatch;
    private String fullnameLengthError;
    private String phoneLengthErr;
    private String addressLengthErr;

    public AccountCreateError() {
    }

    public AccountCreateError(String emailLengthError, String emailTypeErr, String emailIsExist, String passwordLengthError, String confirmNotMatch, String fullnameLengthError, String phoneLengthErr, String addressLengthErr) {
        this.emailLengthError = emailLengthError;
        this.emailTypeErr = emailTypeErr;
        this.emailIsExist = emailIsExist;
        this.passwordLengthError = passwordLengthError;
        this.confirmNotMatch = confirmNotMatch;
        this.fullnameLengthError = fullnameLengthError;
        this.phoneLengthErr = phoneLengthErr;
        this.addressLengthErr = addressLengthErr;
    }

    public String getEmailLengthError() {
        return emailLengthError;
    }

    public void setEmailLengthError(String emailLengthError) {
        this.emailLengthError = emailLengthError;
    }

    public String getEmailTypeErr() {
        return emailTypeErr;
    }

    public void setEmailTypeErr(String emailTypeErr) {
        this.emailTypeErr = emailTypeErr;
    }

    public String getEmailIsExist() {
        return emailIsExist;
    }

    public void setEmailIsExist(String emailIsExist) {
        this.emailIsExist = emailIsExist;
    }

    public String getPasswordLengthError() {
        return passwordLengthError;
    }

    public void setPasswordLengthError(String passwordLengthError) {
        this.passwordLengthError = passwordLengthError;
    }

    public String getConfirmNotMatch() {
        return confirmNotMatch;
    }

    public void setConfirmNotMatch(String confirmNotMatch) {
        this.confirmNotMatch = confirmNotMatch;
    }

    public String getFullnameLengthError() {
        return fullnameLengthError;
    }

    public void setFullnameLengthError(String fullnameLengthError) {
        this.fullnameLengthError = fullnameLengthError;
    }

    public String getPhoneLengthErr() {
        return phoneLengthErr;
    }

    public void setPhoneLengthErr(String phoneLengthErr) {
        this.phoneLengthErr = phoneLengthErr;
    }

    public String getAddressLengthErr() {
        return addressLengthErr;
    }

    public void setAddressLengthErr(String addressLengthErr) {
        this.addressLengthErr = addressLengthErr;
    }

}
