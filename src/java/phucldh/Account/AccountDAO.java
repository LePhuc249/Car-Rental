/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Account;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class AccountDAO implements Serializable{
    
    public AccountDTO getAccount(String id, String password) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select emailAccount, passwordAccount, fullnameAccount, phoneAccount, "
                        + "addressAccount, statusAccount, codeActive, roleID From Account "
                        + "Where emailAccount = ? And passwordAccount = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                stm.setString(2, password);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String email = rs.getString("emailAccount");
                    String pass = rs.getString("passwordAccount");
                    String fullname = rs.getString("fullnameAccount");
                    String phone = rs.getString("phoneAccount");
                    String address = rs.getString("addressAccount");
                    String status = rs.getString("statusAccount");
                    String code = rs.getString("codeActive");
                    String roleID = rs.getString("roleID");
                    AccountDTO dtoAccount = new AccountDTO(email, password, fullname, phone, address, status, roleID);
                    return dtoAccount;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public String getRole(String email) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String role = "";
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select emailAccount, passwordAccount, fullnameAccount, phoneAccount, "
                        + "addressAccount, statusAccount, codeActive, roleID From Account "
                        + "Where emailAccount = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, email);
                rs = stm.executeQuery();
                if (rs.next()) {
                    role = rs.getString("roleID");
                    return role;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public boolean createAccount(AccountDTO dtoAccount) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Insert into Account (emailAccount, passwordAccount, fullnameAccount, phoneAccount, "
                        + "addressAccount, roleID) Values (?, ?, ?, ?, ?, ?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoAccount.getEmail());
                stm.setString(2, dtoAccount.getPassword());
                stm.setString(3, dtoAccount.getFullName());
                stm.setString(4, dtoAccount.getPhone());
                stm.setString(5, dtoAccount.getAddress());
                stm.setString(6, dtoAccount.getRoleID());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public String getVerifyCodeByAccountID(String accID) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String code = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT codeActive FROM Account Where emailAccount = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, accID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    code = rs.getString("codeActive");
                }
                return code;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return code;
    }
    
    public boolean updateVerifyCodeByAccountID(String accID, String codeUpdate) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Account set codeActive = ? Where emailAccount = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, codeUpdate);
                stm.setString(2, accID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public boolean clearVerifyCodeByAccountID(String accID) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Account set codeActive = NULL Where emailAccount = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, accID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public boolean updateStatusToActive(String accID) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Account set statusAccount = 'Active' Where emailAccount = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, accID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
}
