/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrder;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class UserOrderDTO implements Serializable {

    private String orderID;
    private String customerID;
    private String rentalDate;
    private String returnDate;
    private Date orderDate;
    private String status;
    private float totalPrice;
    private String couponID;

    public UserOrderDTO() {
    }

    public UserOrderDTO(String orderID, String rentalDate, String returnDate, String status) {
        this.orderID = orderID;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
        this.status = status;
    }

    public UserOrderDTO(String orderID, String customerID, String rentalDate, String returnDate, String status, float totalPrice) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
        this.status = status;
        this.totalPrice = totalPrice;
    }

    public UserOrderDTO(String orderID, String rentalDate, String returnDate, Date orderDate, String status, float totalPrice, String couponID) {
        this.orderID = orderID;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
        this.orderDate = orderDate;
        this.status = status;
        this.totalPrice = totalPrice;
        this.couponID = couponID;
    }

    public UserOrderDTO(String orderID, String customerID, String rentalDate, String returnDate, String status, float totalPrice, String couponID) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
        this.status = status;
        this.totalPrice = totalPrice;
        this.couponID = couponID;
    }

    public UserOrderDTO(String orderID, String customerID, String rentalDate, String returnDate, Date orderDate, String status, float totalPrice, String couponID) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
        this.orderDate = orderDate;
        this.status = status;
        this.totalPrice = totalPrice;
        this.couponID = couponID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getRentalDate() {
        return rentalDate;
    }

    public void setRentalDate(String rentalDate) {
        this.rentalDate = rentalDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCouponID() {
        return couponID;
    }

    public void setCouponID(String couponID) {
        this.couponID = couponID;
    }

}
