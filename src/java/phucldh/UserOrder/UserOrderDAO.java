/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrder;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class UserOrderDAO implements Serializable {

    private List<UserOrderDTO> listAllUserOrder;

    public List<UserOrderDTO> ListAllOrder() throws NamingException, SQLException {
        return listAllUserOrder;
    }

    private List<UserOrderDTO> listOrderOfUser;

    public List<UserOrderDTO> ListOrderOfUser() throws NamingException, SQLException {
        return listOrderOfUser;
    }

    private List<String> listCouponsID;

    public List<String> ListCouponsHaveUsed() throws NamingException, SQLException {
        return listCouponsID;
    }

    private List<UserOrderDTO> listHistory;

    public List<UserOrderDTO> ListHistory() throws NamingException, SQLException {
        return listHistory;
    }

    public void getAllListOrder() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, rentalDate, returnDate, status FROM UserOrder";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    String rentalDate = rs.getString("rentalDate");
                    String returnDate = rs.getString("returnDate");
                    String status = rs.getString("status");
                    UserOrderDTO dto = new UserOrderDTO(id, rentalDate, returnDate, status);
                    if (this.listAllUserOrder == null) {
                        this.listAllUserOrder = new ArrayList<>();
                    }
                    this.listAllUserOrder.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void getListOrderOfUser(String idCus) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, rentalDate, returnDate, createDate, "
                        + "status, totalPrice, couponID FROM UserOrder "
                        + "Where customerID = ? Order by createDate DESC";
                stm = conn.prepareStatement(sql);
                stm.setString(1, idCus);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    String rentalDate = rs.getString("rentalDate");
                    String returnDate = rs.getString("returnDate");
                    Date orderDate = rs.getDate("createDate");
                    String status = rs.getString("status");
                    float totalPrice = rs.getFloat("totalPrice");
                    String couponID = rs.getString("couponID");
                    UserOrderDTO dto = new UserOrderDTO(id, rentalDate, returnDate, orderDate, status, totalPrice, couponID);
                    if (this.listOrderOfUser == null) {
                        this.listOrderOfUser = new ArrayList<>();
                    }
                    this.listOrderOfUser.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean updateStatusToFinish(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Fi' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateStatusToCancel(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Ca' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateStatusToConfirm(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Co' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateStatusToHiring(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Hi' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean createNewOrder(UserOrderDTO dtoOrder) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO UserOrder "
                        + "(orderID, customerID, rentalDate, returnDate, status, totalPrice, couponID) "
                        + "VALUES (?,?,?,?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoOrder.getOrderID());
                stm.setString(2, dtoOrder.getCustomerID());
                stm.setString(3, dtoOrder.getRentalDate());
                stm.setString(4, dtoOrder.getReturnDate());
                stm.setString(5, dtoOrder.getStatus());
                stm.setFloat(6, dtoOrder.getTotalPrice());
                stm.setString(7, dtoOrder.getCouponID());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean createNewOrderWithOutCoupons(UserOrderDTO dtoOrder) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO UserOrder "
                        + "(orderID, customerID, rentalDate, returnDate, status, totalPrice) "
                        + "VALUES (?,?,?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoOrder.getOrderID());
                stm.setString(2, dtoOrder.getCustomerID());
                stm.setString(3, dtoOrder.getRentalDate());
                stm.setString(4, dtoOrder.getReturnDate());
                stm.setString(5, dtoOrder.getStatus());
                stm.setFloat(6, dtoOrder.getTotalPrice());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean deleteOrder(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Ca' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public String getStatusOrder(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String status = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT status FROM UserOrder Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    status = rs.getString("status");
                }
                return status;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return status;
    }

    public UserOrderDTO getOrder(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        UserOrderDTO dto = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, customerID, rentalDate, returnDate, createDate, status, totalPrice, "
                        + "couponID FROM UserOrder Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("orderID");
                    String rentalDate = rs.getString("rentalDate");
                    String returnDate = rs.getString("returnDate");
                    Date orderDate = rs.getDate("createDate");
                    String status = rs.getString("status");
                    float totalPrice = rs.getFloat("totalPrice");
                    String couponID = rs.getString("couponID");
                    dto = new UserOrderDTO(id, rentalDate, returnDate, orderDate, status, totalPrice, couponID);
                }
                return dto;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public void getListCoupons() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT couponID FROM UserOrder";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("couponID");
                    if (this.listCouponsID == null) {
                        this.listCouponsID = new ArrayList<>();
                    }
                    this.listCouponsID.add(id);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String getLastOrderByIDByDate() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String id = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID FROM UserOrder WHERE createDate = (SELECT MAX(createDate) FROM UserOrder)";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    id = rs.getString("orderID");
                }
                return id;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public void getListHistory(String customerID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, rentalDate, returnDate, createDate,status, totalPrice, couponID FROM UserOrder Where customerID = ? Order by createDate DESC";
                stm = conn.prepareStatement(sql);
                stm.setString(1, customerID);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    String rentalDate = rs.getString("rentalDate");
                    String returnDate = rs.getString("returnDate");
                    Date orderDate = rs.getDate("createDate");
                    String status = rs.getString("status");
                    float totalPrice = rs.getFloat("totalPrice");
                    String couponID = rs.getString("couponID");
                    UserOrderDTO dto = new UserOrderDTO(id, rentalDate, returnDate, orderDate, status, totalPrice, couponID);
                    if (this.listHistory == null) {
                        this.listHistory = new ArrayList<>();
                    }
                    this.listHistory.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
