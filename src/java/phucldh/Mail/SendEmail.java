/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Mail;

import java.util.Properties;
import java.util.Random;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import phucldh.Account.AccountDTO;

/**
 *
 * @author Admin
 */
public class SendEmail {

    public String getCodeRandom() {
        Random randomObject = new Random();
        int number = randomObject.nextInt(999999);
        return String.format("%06d", number);
    }

    public boolean sendEmail(AccountDTO dtoAccount) {
        boolean test = false;
        String toEmail = dtoAccount.getEmail();
        String fromEmail = "phucldh.work@gmail.com";
        String password = "hoangphuc3393ABC";
        try {
            Properties proper = new Properties();
            proper.setProperty("mail.smtp.host", "smtp.gmail.com");
            proper.setProperty("mail.smtp.port", "587");
            proper.setProperty("mail.smtp.auth", "true");
            proper.setProperty("mail.smtp.starttls.enable", "true");
            proper.put("mail.smtp.socketFactory.port", "587");
            proper.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            Session session = Session.getInstance(proper, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromEmail, password);
                }
            });
            Message mess = new MimeMessage(session);
            mess.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            mess.setSubject("User Email Verification");
            mess.setText("Registered successfully.Please verify your account using this code: " + dtoAccount.getCode());
            Transport.send(mess);
            test = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return test;
    }
}
