/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrderDetail;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class UserOrderDetailDTO implements Serializable {

    private String orderLineID;
    private String orderID;
    private String carID;
    private String carProperty;
    private String createDate;

    public UserOrderDetailDTO() {
    }

    public UserOrderDetailDTO(String orderLineID, String orderID, String carID) {
        this.orderLineID = orderLineID;
        this.orderID = orderID;
        this.carID = carID;
    }

    public UserOrderDetailDTO(String orderLineID, String orderID, String carID, String carProperty) {
        this.orderLineID = orderLineID;
        this.orderID = orderID;
        this.carID = carID;
        this.carProperty = carProperty;
    }

    public UserOrderDetailDTO(String orderLineID, String orderID, String carID, String carProperty, String createDate) {
        this.orderLineID = orderLineID;
        this.orderID = orderID;
        this.carID = carID;
        this.carProperty = carProperty;
        this.createDate = createDate;
    }

    public String getCarProperty() {
        return carProperty;
    }

    public void setCarProperty(String carProperty) {
        this.carProperty = carProperty;
    }

    public String getOrderLineID() {
        return orderLineID;
    }

    public void setOrderLineID(String orderLineID) {
        this.orderLineID = orderLineID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}
