/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrderDetail;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;
import phucldh.UserOrder.UserOrderDTO;

/**
 *
 * @author Admin
 */
public class UserOrderDetailDAO implements Serializable{
    
    private List<UserOrderDetailDTO> listItem;
    
    public List<UserOrderDetailDTO> ListItemInOrder() throws NamingException, SQLException{
        return listItem;
    }
    
    private List<UserOrderDetailDTO> listItemSearch;
    
    public List<UserOrderDetailDTO> ListItemInSearch() throws NamingException, SQLException{
        return listItemSearch;
    }
    
    public void getItem(String orderIDGet) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID, orderID, carID, carProperty FROM UserOrderDetail Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderIDGet);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String lineID = rs.getString("orderLineID");
                    String orderID = rs.getString("orderID");
                    String carID = rs.getString("carID");
                    String property = rs.getString("carProperty");
                    UserOrderDetailDTO dto = new UserOrderDetailDTO(lineID, orderID, carID,property);
                    if (this.listItem == null) {
                        this.listItem = new ArrayList<>();
                    }
                    this.listItem.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void getItemByOrderIDAndDetail(String orderIDGet, String CarProperty) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID, orderID, carID, carProperty FROM UserOrderDetail Where orderID = ? AND "
                        + "carProperty LIKE ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderIDGet);
                stm.setString(2,"%" + CarProperty + "%");
                rs = stm.executeQuery();
                while (rs.next()) {
                    String lineID = rs.getString("orderLineID");
                    String orderID = rs.getString("orderID");
                    String carID = rs.getString("carID");
                    String property = rs.getString("carProperty");
                    UserOrderDetailDTO dto = new UserOrderDetailDTO(lineID, orderID, carID,property);
                    if (this.listItemSearch == null) {
                        this.listItemSearch = new ArrayList<>();
                    }
                    this.listItemSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public boolean createOrderDetail(UserOrderDetailDTO dtoUserOrderDetail) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO UserOrderDetail (orderLineID, orderID, carID, carProperty) "
                        + "VALUES (?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoUserOrderDetail.getOrderLineID());
                stm.setString(2, dtoUserOrderDetail.getOrderID());
                stm.setString(3, dtoUserOrderDetail.getCarID());
                stm.setString(4, dtoUserOrderDetail.getCarProperty());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public UserOrderDetailDTO getUserOrderDetail(String idOfDetail) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID, orderID, carID, carProperty FROM UserOrderDetail Where orderLineID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, idOfDetail);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String lineID = rs.getString("orderLineID");
                    String orderID = rs.getString("orderID");
                    String carID = rs.getString("carID");
                    String property = rs.getString("carProperty");
                    UserOrderDetailDTO dto = new UserOrderDetailDTO(lineID, orderID, carID, property);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public String getLastOrderDetailByIDByDate() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String id = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID FROM UserOrderDetail WHERE createDate = (SELECT MAX(createDate) FROM UserOrderDetail) ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    id = rs.getString("orderLineID");
                }
                return id;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
}
