/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.CarType;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class CarTypeDTO implements Serializable {

    private String typeID;
    private String typeName;
    private Date dateCreate;
    private boolean isDeleted;

    public CarTypeDTO() {
    }

    public CarTypeDTO(String typeID, String typeName) {
        this.typeID = typeID;
        this.typeName = typeName;
    }

    public CarTypeDTO(String typeID, String typeName, boolean isDeleted) {
        this.typeID = typeID;
        this.typeName = typeName;
        this.isDeleted = isDeleted;
    }

    public CarTypeDTO(String typeID, String typeName, Date dateCreate, boolean isDeleted) {
        this.typeID = typeID;
        this.typeName = typeName;
        this.dateCreate = dateCreate;
        this.isDeleted = isDeleted;
    }

    public String getTypeID() {
        return typeID;
    }

    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
