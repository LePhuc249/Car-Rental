/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.CarType;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class CarTypeDAO implements Serializable{
    
    private List<CarTypeDTO> listCarType;

    public List<CarTypeDTO> getListType() throws NamingException, SQLException {
        return listCarType;
    }
    
    public void getFullType() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carTypeID, carTypeName FROM CarType";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("carTypeID");
                    String name = rs.getString("carTypeName");
                    CarTypeDTO dto = new CarTypeDTO(id, name);
                    if (this.listCarType == null) {
                        this.listCarType = new ArrayList<>();
                    }
                    this.listCarType.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
