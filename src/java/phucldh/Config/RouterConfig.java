/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Admin
 */
public class RouterConfig implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try (InputStream input = new FileInputStream(System.getenv("PAGE_CONFIG_LAB3"))) {
            Properties properties = new Properties();
            properties.load(input);
            sce.getServletContext().setAttribute("ROUTE_CONFIG", properties);
        } catch (IOException e) {
            sce.getServletContext().log("Router Config IO: " + e.getMessage());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Servlet context have been destroyed");
    }

}
