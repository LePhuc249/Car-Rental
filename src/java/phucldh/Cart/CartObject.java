/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Cart;

import java.io.Serializable;
import java.util.HashMap;
import phucldh.Car.CarDTO;
import phucldh.CarCategory.CarCategoryDTO;

/**
 *
 * @author Admin
 */
public class CartObject implements Serializable {
    
    private String customerName;
    private HashMap<String, CarCategoryDTO> shoppingCart;

    public CartObject() {
        this.customerName = "Guest";
        this.shoppingCart = new HashMap<>();
    }

    public CartObject(String customerNameO) {
        this.customerName = customerName;
        this.shoppingCart = new HashMap<>();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public HashMap<String, CarCategoryDTO> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(HashMap<String, CarCategoryDTO> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void addToCart(CarCategoryDTO car) throws Exception {
        if (this.shoppingCart.containsKey(car.getCateID())) {
            int quantity = this.shoppingCart.get(car.getCateID()).getQuantity() + 1;
            car.setQuantity(quantity);
        }
        this.shoppingCart.put(car.getCateID(), car);
    }

    public void update(String id, int quantity) throws Exception {
        if (this.shoppingCart.containsKey(id)) {
            this.shoppingCart.get(id).setQuantity(quantity);
        }
    }

    public void remove(String id) throws Exception {
        if (this.shoppingCart.containsKey(id)) {
            this.shoppingCart.remove(id);
        }
    }

    public float getTotal() {
        float total = 0;
        for (CarCategoryDTO car : this.shoppingCart.values()) {
            total += car.getCatePrice() * car.getQuantity();
        }
        return total;
    }
}
