/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrderStatus;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class UserOrderStatusDTO implements Serializable {

    private String statusID;
    private String statusName;
    private Date createDate;
    private boolean isDelete;

    public UserOrderStatusDTO() {
    }

    public UserOrderStatusDTO(String statusID, String statusName) {
        this.statusID = statusID;
        this.statusName = statusName;
    }

    public UserOrderStatusDTO(String statusID, String statusName, Date createDate, boolean isDelete) {
        this.statusID = statusID;
        this.statusName = statusName;
        this.createDate = createDate;
        this.isDelete = isDelete;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

}
