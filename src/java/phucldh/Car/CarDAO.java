/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Car;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class CarDAO implements Serializable {

    private List<CarDTO> listCarFreeCanRent;

    public List<CarDTO> getListCarFreeCanRent() throws NamingException, SQLException {
        return listCarFreeCanRent;
    }

    // Get List car free can be rent 
    public void getCarFreeCanRent(String cateID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID, VIN, cateCarID, status, isDeleted FROM Car "
                        + "WHERE isDeleted = 0 AND status = 'Fe' AND cateCarID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, cateID);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("carID");
                    String vin = rs.getString("VIN");
                    String cateCarID = rs.getString("cateCarID");
                    String status = rs.getString("status");
                    boolean delete = rs.getBoolean("isDeleted");
                    CarDTO dto = new CarDTO(id, vin, cateCarID, status, delete);
                    if (this.listCarFreeCanRent == null) {
                        this.listCarFreeCanRent = new ArrayList<>();
                    }
                    this.listCarFreeCanRent.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    // Count total number car of one category
    public int getNumberCarOfCarCategoryTotal(String cateId) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int count = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID FROM Car WHERE isDeleted = 0 AND cateCarID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, cateId);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++; 
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    // Count number car busy of one category 
    public int getNumberCarOfCarCategoryBusy(String cateId) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int count = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID FROM Car WHERE isDeleted = 0 AND cateCarID = ? AND "
                        + " status = 'Bu'";
                stm = conn.prepareStatement(sql);
                stm.setString(1, cateId);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++; 
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    public boolean updateStatusOfCarToFree(String id) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        int count = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Car SET status = 'Fe' WHERE carID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public boolean updateStatusOfCarToBusy(String id) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        int count = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Car SET status = 'Bu' WHERE carID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public CarDTO getCarRent(String cateID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        CarDTO dtoCar = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID, VIN, cateCarID, status, isDeleted FROM Car "
                        + "WHERE isDeleted = 0 AND status = 'Fe' AND cateCarID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, cateID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("carID");
                    String vin = rs.getString("VIN");
                    String cateCarID = rs.getString("cateCarID");
                    String status = rs.getString("status");
                    boolean delete = rs.getBoolean("isDeleted");
                    CarDTO dto = new CarDTO(id, vin, cateCarID, status, delete);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public CarDTO getCarRentByCarID(String cateID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        CarDTO dtoCar = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID, VIN, cateCarID, status, isDeleted FROM Car "
                        + "WHERE isDeleted = 0 AND status = 'Fe' AND carID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, cateID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("carID");
                    String vin = rs.getString("VIN");
                    String cateCarID = rs.getString("cateCarID");
                    String status = rs.getString("status");
                    boolean delete = rs.getBoolean("isDeleted");
                    CarDTO dto = new CarDTO(id, vin, cateCarID, status, delete);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public CarDTO getCarByCarID(String cateID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        CarDTO dtoCar = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID, VIN, cateCarID, status, isDeleted FROM Car "
                        + "WHERE isDeleted = 0 AND carID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, cateID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("carID");
                    String vin = rs.getString("VIN");
                    String cateCarID = rs.getString("cateCarID");
                    String status = rs.getString("status");
                    boolean delete = rs.getBoolean("isDeleted");
                    CarDTO dto = new CarDTO(id, vin, cateCarID, status, delete);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public String getCarCategory(String id) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String carCate = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cateCarID FROM Car WHERE carID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                rs = stm.executeQuery();
                if (rs.next()) {
                    carCate = rs.getString("cateCarID");
                    return carCate;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public String getCarFree(String id ) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String carID = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT carID FROM Car "
                        + "WHERE isDeleted = 0 AND status = 'Fe' AND cateCarID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                rs = stm.executeQuery();
                if (rs.next()) {
                    carID = rs.getString("carID");
                    return carID;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
}
