/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Car;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class CarDTO implements Serializable {

    private String carID;
    private String VIN;
    private String cateCarID;
    private Date createDate;
    private String status;
    private boolean isDeleted;

    public CarDTO() {
    }

    public CarDTO(String carID, String status) {
        this.carID = carID;
        this.status = status;
    }

    public CarDTO(String carID, String VIN, String status) {
        this.carID = carID;
        this.VIN = VIN;
        this.status = status;
    }

    public CarDTO(String carID, String VIN, String cateCarID, String status) {
        this.carID = carID;
        this.VIN = VIN;
        this.cateCarID = cateCarID;
        this.status = status;
    }

    public CarDTO(String carID, String VIN, String cateCarID, String status, boolean isDeleted) {
        this.carID = carID;
        this.VIN = VIN;
        this.cateCarID = cateCarID;
        this.status = status;
        this.isDeleted = isDeleted;
    }

    public CarDTO(String carID, String VIN, String cateCarID, Date createDate, String status, boolean isDeleted) {
        this.carID = carID;
        this.VIN = VIN;
        this.cateCarID = cateCarID;
        this.createDate = createDate;
        this.status = status;
        this.isDeleted = isDeleted;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getCateCarID() {
        return cateCarID;
    }

    public void setCateCarID(String cateCarID) {
        this.cateCarID = cateCarID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
