/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.CarCategory;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class CarCategoryDAO implements Serializable {

    private List<CarCategoryDTO> listCarSearch;

    public List<CarCategoryDTO> getListCategoryForSearch() throws NamingException, SQLException {
        return listCarSearch;
    }

    private List<CarCategoryDTO> listCarAll;

    public List<CarCategoryDTO> getListCategoryAll() throws NamingException, SQLException {
        return listCarAll;
    }

    public CarCategoryDTO getCarCate(String id) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        CarCategoryDTO dtoCarCategory = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cateCarID, cateCarModel, cateCarMake, cateCarColor, "
                        + "cateCarYear, cateCarPrice, cateCarType, isDeleted "
                        + "FROM CarCategory Where cateCarID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String idCate = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String make = rs.getString("cateCarMake");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    boolean delete = rs.getBoolean("isDeleted");
                    CarCategoryDTO dto = new CarCategoryDTO(idCate, model, make, color, year, price, type, delete);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public String getNameByID(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String name = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cateCarModel FROM CarCategory Where cateCarID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    name = rs.getString("cateCarModel");
                    return name;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return name;
    }

    public void getAllCarCate() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cateCarModel FROM CarCategory Where cateCarID = ?";
                stm = conn.prepareStatement(sql);

                rs = stm.executeQuery();
                while (rs.next()) {

                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void getCarCateSearchByDateAndAmount(String rentalDate, String returnDate, int amount, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType  "
                        + "HAVING COUNT(c.VIN) >= ? "
                        + "ORDER BY cc.createDate  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setInt(3, amount);
                stm.setInt(4, offset);
                stm.setInt(5, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    int quantity = rs.getInt("carCount");
                    CarCategoryDTO dto = new CarCategoryDTO(id,model, color, year, price, type, quantity);
                    if (this.listCarSearch == null) {
                        this.listCarSearch = new ArrayList<>();
                    }
                    this.listCarSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getCounterCarCateSearchByDateAndAmount(String rentalDate, String returnDate, int amount) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType  "
                        + "HAVING COUNT(c.VIN) >= ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setInt(3, amount);
                rs = stm.executeQuery();
                while (rs.next()) {
                    counter++;
                }
                return counter;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public void getCarCateFull(int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "	AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "                    AND uo.rentalDate <= CURRENT_TIMESTAMP AND uo.returnDate >= CURRENT_TIMESTAMP) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= 1 "
                        + "ORDER BY cc.createDate  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                stm = conn.prepareStatement(sql);
                stm.setInt(1, offset);
                stm.setInt(2, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    int quantity = rs.getInt("carCount");
                    CarCategoryDTO dto = new CarCategoryDTO(id,model, color, year, price, type, quantity);
                    if (this.listCarAll == null) {
                        this.listCarAll = new ArrayList<>();
                    }
                    this.listCarAll.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getCounterCarCateFull() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "	AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "                    AND uo.rentalDate <= CURRENT_TIMESTAMP AND uo.returnDate >= CURRENT_TIMESTAMP) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= 1 ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    counter++;
                }
                return counter;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    public void getCarCateFullForAdmin(int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "	AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "                    AND uo.rentalDate <= CURRENT_TIMESTAMP AND uo.returnDate >= CURRENT_TIMESTAMP) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= 0 "
                        + "ORDER BY cc.createDate  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                stm = conn.prepareStatement(sql);
                stm.setInt(1, offset);
                stm.setInt(2, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    int quantity = rs.getInt("carCount");
                    CarCategoryDTO dto = new CarCategoryDTO(id,model, color, year, price, type, quantity);
                    if (this.listCarAll == null) {
                        this.listCarAll = new ArrayList<>();
                    }
                    this.listCarAll.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getCounterCarCateFullForAdmin() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "	AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "                    AND uo.rentalDate <= CURRENT_TIMESTAMP AND uo.returnDate >= CURRENT_TIMESTAMP) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= 0 ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    counter++;
                }
                return counter;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    public int getTotalQuantityItemByID(String id) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int amount = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "	AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "                    AND uo.rentalDate <= CURRENT_TIMESTAMP AND uo.returnDate >= CURRENT_TIMESTAMP) "
                        + "     AND cc.cateCarID = ? "
                        + "GROUP BY cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                rs = stm.executeQuery();
                if (rs.next()) {
                    amount = rs.getInt("carCount");
                }
                return amount;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    public void getCarCateSearchByDateAndAmountAndModel(String carModel,String rentalDate, String returnDate, int amount, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "	AND cc.cateCarModel LIKE ? "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= ? "
                        + "ORDER BY cc.createDate  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setString(3,"%" + carModel + "%");
                stm.setInt(4, amount);
                stm.setInt(5, offset);
                stm.setInt(6, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    int quantity = rs.getInt("carCount");
                    CarCategoryDTO dto = new CarCategoryDTO(id, model, color, year, price, type, quantity);
                    if (this.listCarSearch == null) {
                        this.listCarSearch = new ArrayList<>();
                    }
                    this.listCarSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getCounterCarCateSearchByDateAndAmountAndModel(String carModel, String rentalDate, String returnDate, int amount) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "	AND cc.cateCarModel LIKE ?  "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setString(3, "%" + carModel + "%");
                stm.setInt(4, amount);
                rs = stm.executeQuery();
                while (rs.next()) {
                    counter++;
                }
                return counter;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    public void getCarCateSearchByDateAndAmountAndType(String carType, String rentalDate, String returnDate, int amount, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "	AND ct.carTypeID IN (?)  "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType  "
                        + "HAVING COUNT(c.VIN) >= ? "
                        + "ORDER BY cc.createDate  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setString(3, carType);
                stm.setInt(4, amount);
                stm.setInt(5, offset);
                stm.setInt(6, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    int quantity = rs.getInt("carCount");
                    CarCategoryDTO dto = new CarCategoryDTO(id,model, color, year, price, type, quantity);
                    if (this.listCarSearch == null) {
                        this.listCarSearch = new ArrayList<>();
                    }
                    this.listCarSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getCounterCarCateSearchByDateAndAmountAndType(String carType, String rentalDate, String returnDate, int amount) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "	AND ct.carTypeID IN (?) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType  "
                        + "HAVING COUNT(c.VIN) >= ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setString(3, carType);
                stm.setInt(4, amount);
                rs = stm.executeQuery();
                while (rs.next()) {
                    counter++;
                }
                return counter;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
    
    public void getCarCateSearchByDateAndAmountAndTypeAndModel(String carModel, String carType, String rentalDate, String returnDate, int amount, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "	AND ((cc.cateCarModel LIKE ?) OR (ct.carTypeID IN (?))) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= ? "
                        + "ORDER BY cc.createDate  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setString(3, "%" + carModel + "%");
                stm.setString(4, carType);
                stm.setInt(5, amount);
                stm.setInt(6, offset);
                stm.setInt(7, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("cateCarID");
                    String model = rs.getString("cateCarModel");
                    String color = rs.getString("cateCarColor");
                    String year = rs.getString("cateCarYear");
                    float price = rs.getFloat("cateCarPrice");
                    String type = rs.getString("cateCarType");
                    int quantity = rs.getInt("carCount");
                    CarCategoryDTO dto = new CarCategoryDTO(id,model, color, year, price, type, quantity);
                    if (this.listCarSearch == null) {
                        this.listCarSearch = new ArrayList<>();
                    }
                    this.listCarSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getCounterCarCateSearchByDateAndAmountAndTypeAndModel(String carModel, String carType, String rentalDate, String returnDate, int amount) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int counter = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType,COUNT(c.VIN) carCount "
                        + "FROM dbo.CarType ct, dbo.CarCategory cc, dbo.Car c "
                        + "WHERE ct.carTypeID = cc.cateCarType "
                        + "     AND cc.cateCarID = c.cateCarID "
                        + "	AND c.carID NOT IN (SELECT uod.carID FROM dbo.UserOrderDetail uod, dbo.UserOrder uo WHERE uod.orderID = uo.orderID "
                        + "						  AND uo.rentalDate <= ? AND uo.returnDate >= ?) "
                        + "	AND ((cc.cateCarModel LIKE ?) OR (ct.carTypeID IN (?))) "
                        + "GROUP BY cc.cateCarID, cc.cateCarModel,cc.cateCarColor,cc.cateCarYear,cc.cateCarPrice,cc.createDate,cc.cateCarType "
                        + "HAVING COUNT(c.VIN) >= ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, returnDate);
                stm.setString(2, rentalDate);
                stm.setString(3, "%" + carModel + "%");
                stm.setString(4, carType);
                stm.setInt(5, amount);
                rs = stm.executeQuery();
                while (rs.next()) {
                    counter++;
                }
                return counter;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }
}
