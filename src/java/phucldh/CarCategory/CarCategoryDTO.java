/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.CarCategory;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class CarCategoryDTO implements Serializable {

    private String cateID;
    private String cateModel;
    private String cateMake;
    private String cateColor;
    private String cateYear;
    private float catePrice;
    private String cateType;
    private Date createDate;
    private boolean isDelete;
    private int quantity;

    public CarCategoryDTO() {
    }

    public CarCategoryDTO(String cateID) {
        this.cateID = cateID;
    }

    public CarCategoryDTO(String cateID, String cateModel, String cateMake, String cateColor, String cateYear, float catePrice, String cateType) {
        this.cateID = cateID;
        this.cateModel = cateModel;
        this.cateMake = cateMake;
        this.cateColor = cateColor;
        this.cateYear = cateYear;
        this.catePrice = catePrice;
        this.cateType = cateType;
    }

    public CarCategoryDTO(String cateModel, String cateColor, String cateYear, float catePrice, String cateType, int quantity) {
        this.cateModel = cateModel;
        this.cateColor = cateColor;
        this.cateYear = cateYear;
        this.catePrice = catePrice;
        this.cateType = cateType;
        this.quantity = quantity;
    }

    public CarCategoryDTO(String cateID, String cateModel, String cateMake, String cateColor, String cateYear, float catePrice, String cateType, Date createDate) {
        this.cateID = cateID;
        this.cateModel = cateModel;
        this.cateMake = cateMake;
        this.cateColor = cateColor;
        this.cateYear = cateYear;
        this.catePrice = catePrice;
        this.cateType = cateType;
        this.createDate = createDate;
    }

    public CarCategoryDTO(String cateID, String cateModel, String cateColor, String cateYear, float catePrice, String cateType, int quantity) {
        this.cateID = cateID;
        this.cateModel = cateModel;
        this.cateColor = cateColor;
        this.cateYear = cateYear;
        this.catePrice = catePrice;
        this.cateType = cateType;
        this.quantity = quantity;
    }

    public CarCategoryDTO(String cateID, String cateModel, String cateMake, String cateColor, String cateYear, float catePrice, String cateType, boolean isDelete) {
        this.cateID = cateID;
        this.cateModel = cateModel;
        this.cateMake = cateMake;
        this.cateColor = cateColor;
        this.cateYear = cateYear;
        this.catePrice = catePrice;
        this.cateType = cateType;
        this.isDelete = isDelete;
    }

    public CarCategoryDTO(String cateID, String cateModel, String cateMake, String cateColor, String cateYear, float catePrice, String cateType, Date createDate, boolean isDelete) {
        this.cateID = cateID;
        this.cateModel = cateModel;
        this.cateMake = cateMake;
        this.cateColor = cateColor;
        this.cateYear = cateYear;
        this.catePrice = catePrice;
        this.cateType = cateType;
        this.createDate = createDate;
        this.isDelete = isDelete;
    }

    public String getCateID() {
        return cateID;
    }

    public void setCateID(String cateID) {
        this.cateID = cateID;
    }

    public String getCateModel() {
        return cateModel;
    }

    public void setCateModel(String cateModel) {
        this.cateModel = cateModel;
    }

    public String getCateMake() {
        return cateMake;
    }

    public void setCateMake(String cateMake) {
        this.cateMake = cateMake;
    }

    public String getCateColor() {
        return cateColor;
    }

    public void setCateColor(String cateColor) {
        this.cateColor = cateColor;
    }

    public String getCateYear() {
        return cateYear;
    }

    public void setCateYear(String cateYear) {
        this.cateYear = cateYear;
    }

    public float getCatePrice() {
        return catePrice;
    }

    public void setCatePrice(float catePrice) {
        this.catePrice = catePrice;
    }

    public String getCateType() {
        return cateType;
    }

    public void setCateType(String cateType) {
        this.cateType = cateType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
