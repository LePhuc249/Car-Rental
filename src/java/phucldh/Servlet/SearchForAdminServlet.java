/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SearchForAdminServlet", urlPatterns = {"/SearchForAdminServlet"})
public class SearchForAdminServlet extends HttpServlet {

    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final int RECORD_PER_PAGE = 20;
    private final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        int page = 1;
        int noOfRecord = 0;
        int amount = 0;
        boolean error = false;
        String url = ADMIN_PAGE;
        String searchModel = request.getParameter("txtSearchCar");
        String searchRentalDate = request.getParameter("txtDateRental");
        String searchReturnDate = request.getParameter("txtDateReturn");
        String searchAmount = request.getParameter("txtAmountCar");
        String searchCategory = request.getParameter("cboCategory");
        String pageParam = request.getParameter("page");
        String errorDate = "";
        String dateNow = (java.time.LocalDate.now().toString()).trim();
        Date rentalDateCar = null;
        Date returnDateCar = null;

        try {
            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }
            if (searchAmount != null && searchAmount.length() > 0) {
                amount = Integer.parseInt(searchAmount);
            }
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            CarCategoryDAO daoCarCategory = new CarCategoryDAO();
                            Date now = formater.parse(dateNow);
                            if (!searchRentalDate.equalsIgnoreCase("")) {
                                rentalDateCar = formater.parse(searchRentalDate);
                            }
                            if (!searchReturnDate.equalsIgnoreCase("")) {
                                returnDateCar = formater.parse(searchReturnDate);
                            }
                            if (rentalDateCar != null && returnDateCar != null) {
                                request.setAttribute("ERRORDATE", null);
                                if (returnDateCar.compareTo(rentalDateCar) < 0) {
                                    error = true;
                                    errorDate = "The date rental must before the date return";
                                }
                                if (returnDateCar.compareTo(now) < 0) {
                                    error = true;
                                    errorDate = "The date return must after today";
                                }
                                if (rentalDateCar.compareTo(now) <= 0) {
                                    error = true;
                                    errorDate = "The date rental must before today";
                                }
                                if (returnDateCar.compareTo(rentalDateCar) == 0) {
                                    error = true;
                                    errorDate = "The date rental must different from date return";
                                }
                                if (rentalDateCar.compareTo(now) < 0 && returnDateCar.compareTo(now) > 0) {
                                    error = true;
                                    errorDate = "The date rental must before today";
                                }
                            }
                            if (error) {
                                request.setAttribute("ERRORDATE", errorDate);
                            } else {
                                if (searchRentalDate.trim().isEmpty() && searchReturnDate.trim().isEmpty() && searchAmount.trim().isEmpty() && searchModel.trim().isEmpty() && searchCategory.trim().equalsIgnoreCase("")) {
                                    context.setAttribute("ITEMLIST", null);
                                    daoCarCategory.getCarCateFullForAdmin((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryAll();
                                    context.setAttribute("ITEMLIST", listCar);
                                    noOfRecord = daoCarCategory.getCounterCarCateFullForAdmin();
                                }
                                if (searchRentalDate.trim().equalsIgnoreCase("") && searchReturnDate.trim().equalsIgnoreCase("") && searchAmount.trim().equalsIgnoreCase("") && searchModel.trim().equalsIgnoreCase("") && searchCategory.trim().equalsIgnoreCase("NULL")) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoCarCategory.getCarCateFullForAdmin((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryAll();
                                    request.setAttribute("SEARCHITEMRESULT", listCar);
                                    noOfRecord = daoCarCategory.getCounterCarCateFullForAdmin();
                                }
                                if (searchCategory.trim().equalsIgnoreCase("NULL") && !searchRentalDate.trim().isEmpty() && !searchReturnDate.trim().isEmpty() && !searchAmount.trim().isEmpty() && searchModel.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoCarCategory.getCarCateSearchByDateAndAmount(searchRentalDate, searchReturnDate, amount, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryForSearch();
                                    request.setAttribute("SEARCHITEMRESULT", listCar);
                                    noOfRecord = daoCarCategory.getCounterCarCateSearchByDateAndAmount(searchRentalDate, searchReturnDate, amount);
                                }
                                if (!searchCategory.trim().equalsIgnoreCase("") && !searchCategory.trim().equalsIgnoreCase("NULL") && !searchRentalDate.trim().isEmpty() && !searchReturnDate.trim().isEmpty() && !searchAmount.trim().isEmpty() && searchModel.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoCarCategory.getCarCateSearchByDateAndAmountAndType(searchCategory, searchRentalDate, searchReturnDate, amount, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryForSearch();
                                    request.setAttribute("SEARCHITEMRESULT", listCar);
                                    noOfRecord = daoCarCategory.getCounterCarCateSearchByDateAndAmountAndType(searchCategory, searchRentalDate, searchReturnDate, amount);
                                }
                                if (searchCategory.trim().equalsIgnoreCase("NULL") && !searchRentalDate.trim().isEmpty() && !searchReturnDate.trim().isEmpty() && !searchAmount.trim().isEmpty() && !searchModel.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoCarCategory.getCarCateSearchByDateAndAmountAndModel(searchModel, searchRentalDate, searchReturnDate, amount, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryForSearch();
                                    request.setAttribute("SEARCHITEMRESULT", listCar);
                                    noOfRecord = daoCarCategory.getCounterCarCateSearchByDateAndAmountAndModel(searchModel, searchRentalDate, searchReturnDate, amount);
                                }
                                if (!searchCategory.trim().equalsIgnoreCase("") && !searchCategory.trim().equalsIgnoreCase("NULL") && !searchRentalDate.trim().isEmpty() && !searchReturnDate.trim().isEmpty() && !searchAmount.trim().isEmpty() && !searchModel.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoCarCategory.getCarCateSearchByDateAndAmountAndTypeAndModel(searchModel, searchCategory, searchRentalDate, searchReturnDate, amount, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryForSearch();
                                    request.setAttribute("SEARCHITEMRESULT", listCar);
                                    noOfRecord = daoCarCategory.getCounterCarCateSearchByDateAndAmountAndTypeAndModel(searchModel, searchCategory, searchRentalDate, searchReturnDate, amount);
                                }
                                if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                                    int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                                    session.setAttribute("NOOFPAGE", noOfPage);
                                    session.setAttribute("CURRENTPAGE", page);
                                }
                            }
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            url = CUSTOMER_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("SearchForAdminServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("SearchForAdminServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("SearchForAdminServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("SearchForAdminServlet_NumberFormatException " + ex.getMessage());
        } catch (ParseException ex) {
            log("SearchForAdminServlet_Parse " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
