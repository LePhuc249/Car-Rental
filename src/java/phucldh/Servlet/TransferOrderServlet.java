/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "TransferOrderServlet", urlPatterns = {"/TransferOrderServlet"})
public class TransferOrderServlet extends HttpServlet {
    
    private final String VIEW_CART_PAGE = "viewCartPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String APPLY_CODE_ACTION = "ApplyCodeServlet";
    private final String CHECK_OUT_ACTION = "OrderServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String action = request.getParameter("btAction");
        String url = VIEW_CART_PAGE;
        String rentalDate = request.getParameter("txtDateWantRental");
        String returnDate = request.getParameter("txtDateWantReturn");
        String couponsCode = request.getParameter("txtCouponCode");
        String totalNow = request.getParameter("txtTotalPrice");

        try {
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (action != null && action.length() > 0) {
                                if (action.equalsIgnoreCase("Apply")) {
                                    url = APPLY_CODE_ACTION;
                                    session.setAttribute("VALUETRANSFERCOUPONSCODE", couponsCode);
                                } else if (action.equalsIgnoreCase("Check Out")) {
                                    url = CHECK_OUT_ACTION;
                                    session.setAttribute("VALUETRANSFERDATERENTAL", rentalDate);
                                    session.setAttribute("VALUETRANSFERDATERETURN", returnDate);
                                    session.setAttribute("VALUETRANSFERCOUPONSCODE", couponsCode);
                                    session.setAttribute("VALUETRANSFERTOTALPRICE", totalNow);
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("TransferOrderServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("TransferOrderServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("TransferOrderServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
