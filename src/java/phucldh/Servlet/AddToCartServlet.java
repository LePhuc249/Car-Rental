/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Car.CarDAO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.Cart.CartObject;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "AddToCartServlet", urlPatterns = {"/AddToCartServlet"})
public class AddToCartServlet extends HttpServlet {

    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        int page = 1;
        int amount = 0;
        String url = CUSTOMER_PAGE;
        String carCateID = request.getParameter("txtCarCateID");
        CarCategoryDTO dtoCarCate = null;
        String rentalDate = request.getParameter("cartDateRental");
        String returnDate = request.getParameter("cartDateReturn");
        String textAmount = request.getParameter("txtAmount");
        String txtCarCate = request.getParameter("cboCategory");
        String searchModel = request.getParameter("txtSearchCar");
        String pageParam = request.getParameter("page");
        boolean error = false;
        String errorMsg = "Please input field rental date, return date and amount before add to cart";

        try {
            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }
            if (textAmount != null && textAmount.length() > 0) {
                amount = Integer.parseInt(textAmount);
            }
            HttpSession session = request.getSession(false);
            session.setAttribute("ERROREMPTYFIELD", null);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (!rentalDate.trim().isEmpty() && !returnDate.trim().isEmpty() && !textAmount.trim().isEmpty()) {
                                CarDAO daoCar = new CarDAO();
                                CarCategoryDAO daoCarCate = new CarCategoryDAO();
                                String customerID = dtoAccount.getEmail();
                                if (customerID != null) {
                                    CartObject shoppingCart = (CartObject) session.getAttribute("shoppingCart");
                                    if (shoppingCart == null) {
                                        session.setAttribute("CARTRENTALDATE", null);
                                        session.setAttribute("CARTRETURNDATE", null);
                                        shoppingCart = new CartObject(customerID);
                                        session.setAttribute("CARTRENTALDATE", rentalDate);
                                        session.setAttribute("CARTRETURNDATE", returnDate);
                                        session.setAttribute("NUMBERDAY", null);
                                        Date rentalDateCar = formater.parse(rentalDate);
                                        Date returnDateCar = formater.parse(returnDate);
                                        long between = returnDateCar.getTime() - rentalDateCar.getTime();
                                        long numberDateTypeLong = between / (1000 * 3600 * 24);
                                        int numberDate = (int) numberDateTypeLong;
                                        session.setAttribute("NUMBERDAY", numberDate);
                                    }
                                    dtoCarCate = daoCarCate.getCarCate(carCateID);
                                    dtoCarCate.setQuantity(1);
                                    shoppingCart.addToCart(dtoCarCate);
                                    session.setAttribute("shoppingCart", shoppingCart);
                                    url = "SearchCarServlet?txtSearchCar=" + searchModel + "&txtDateRental=" + rentalDate
                                            + "&txtDateReturn=" + returnDate + "&txtAmountCar=" + amount
                                            + "&cboCategory=" + txtCarCate + "&page=" + page + "&btAction=SearchForAdmin";
                                }
                            } else if (rentalDate.trim().isEmpty() || returnDate.trim().isEmpty() || textAmount.trim().isEmpty()) {
                                error = true;
                            }
                            if (error) {
                                session.setAttribute("ERROREMPTYFIELD", errorMsg);
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("AddToCartServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("AddToCartServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("AddToCartServlet_NullPointer " + ex.getMessage());
        } catch (Exception ex) {
            log("AddToCartServlet_Exception " + ex.getMessage());
        } finally {
            response.sendRedirect(url);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
