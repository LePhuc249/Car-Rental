/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Car.CarDAO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.CarType.CarTypeDAO;
import phucldh.CarType.CarTypeDTO;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;
import phucldh.UserOrderDetail.UserOrderDetailDAO;
import phucldh.UserOrderDetail.UserOrderDetailDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "StartUpServlet", urlPatterns = {"/StartUpServlet"})
public class StartUpServlet extends HttpServlet {

    private final int RECORD_PER_PAGE = 20;
    private final String DEFAULT_PAGE = "homePage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        int page = 1;
        int noOfRecord = 0;
        String url = DEFAULT_PAGE;
        String pageParam = request.getParameter("page");
        String dateNow = (java.time.LocalDate.now().toString()).trim();

        try {

            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }

            ServletContext context = getServletContext();
            HttpSession session = request.getSession(true);

            UserOrderDAO daoUserOrder = new UserOrderDAO();
            UserOrderDetailDAO daoUserOrderDetail = new UserOrderDetailDAO();
            CarDAO daoCar = new CarDAO();
            CarTypeDAO daoCarType = new CarTypeDAO();
            CouponsDAO daoCoupon = new CouponsDAO();
            Date now = formater.parse(dateNow);

            daoUserOrder.getAllListOrder();
            List<UserOrderDTO> listUserOrder = daoUserOrder.ListAllOrder();
            if (listUserOrder != null) {
                for (UserOrderDTO userOrderDTO : listUserOrder) {
                    String returnDate = userOrderDTO.getReturnDate();
                    String rentalDate = userOrderDTO.getRentalDate();
                    Date rentalDateCar = formater.parse(rentalDate);
                    Date returnDateCar = formater.parse(returnDate);
                    if (now.compareTo(rentalDateCar) < 0 && !userOrderDTO.getStatus().equalsIgnoreCase("Ca")) {
                        boolean updateStatusOrder = daoUserOrder.updateStatusToConfirm(userOrderDTO.getOrderID());
                        if (updateStatusOrder) {
                            daoUserOrderDetail.getItem(userOrderDTO.getOrderID());
                            List<UserOrderDetailDTO> listUserOrderDetail = daoUserOrderDetail.ListItemInOrder();
                            for (UserOrderDetailDTO userOrderDetailDTO : listUserOrderDetail) {
                                daoCar.updateStatusOfCarToBusy(userOrderDetailDTO.getCarID());
                            }
                        }
                    } else if (now.compareTo(rentalDateCar) >= 0 && now.compareTo(returnDateCar) <= 0 && !userOrderDTO.getStatus().equalsIgnoreCase("Ca")) {
                        daoUserOrder.updateStatusToHiring(userOrderDTO.getOrderID());
                    } else if (now.compareTo(returnDateCar) > 0) {
                        boolean updateStatusOrder = daoUserOrder.updateStatusToFinish(userOrderDTO.getOrderID());
                        if (updateStatusOrder) {
                            if (userOrderDTO.getStatus().equals("Fi")) {
                                daoUserOrderDetail.getItem(userOrderDTO.getOrderID());
                                List<UserOrderDetailDTO> listUserOrderDetail = daoUserOrderDetail.ListItemInOrder();
                                for (UserOrderDetailDTO userOrderDetailDTO : listUserOrderDetail) {
                                    daoCar.updateStatusOfCarToFree(userOrderDetailDTO.getCarID());
                                }
                            }
                        }
                    }
                }
            }
            daoCoupon.getListCoupons();
            List<CouponsDTO> listCoupons = daoCoupon.getTheList();
            if (listCoupons != null) {
                for (CouponsDTO listCoupon : listCoupons) {
                    String expDate = listCoupon.getExpirationDate().toString();
                    Date expDateType = formater.parse(expDate);
                    if (expDateType.compareTo(now) > 0) {
                        daoCoupon.updateStatusCoupon(listCoupon.getCouponsID());
                    }
                }
            }

            daoCarType.getFullType();
            List<CarTypeDTO> listCarType = daoCarType.getListType();
            context.setAttribute("CARTYPELIST", listCarType);

            CarCategoryDAO daoCarCategory = new CarCategoryDAO();
            daoCarCategory.getCarCateFull((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
            List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryAll();
            context.setAttribute("ITEMLIST", listCar);
            noOfRecord = daoCarCategory.getCounterCarCateFull();
            if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                session.setAttribute("NOOFPAGE", noOfPage);
                session.setAttribute("CURRENTPAGE", page);
            }

            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                String userEmail = "";
                String password = "";
                for (Cookie cooky : cookies) {
                    if (cooky.getName().equals("USEREMAIL")) {
                        userEmail = cooky.getValue();
                    }
                    if (cooky.getName().equals("PASSWORD")) {
                        password = cooky.getValue();
                    }
                }
                AccountDAO daoAccount = new AccountDAO();
                AccountDTO dtoAccount = daoAccount.getAccount(userEmail, password);
                if (dtoAccount != null) {
                    String roleID = daoAccount.getRole(dtoAccount.getEmail());
                    if (roleID != null && !roleID.equals("")) {
                        RoleDAO daoRole = new RoleDAO();
                        String roleName = daoRole.getRoleName(roleID);
                        if (roleName != null && !roleName.equals("")) {
                            if (roleName.equalsIgnoreCase("Admin")) {
                                url = ADMIN_PAGE;
                            } else if (roleName.equalsIgnoreCase("Customer")) {
                                url = CUSTOMER_PAGE;
                            }
                            session.setAttribute("ACCOUNTOBJECT", dtoAccount);
                            session.setAttribute("ROLENAME", roleName);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            log("StartUpServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("StartUpServlet_Naming " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("StartUpServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("StartUpServlet_NumberFormat " + ex.getMessage());
        } catch (ParseException ex) {
            log("StartUpServlet_Parse " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
