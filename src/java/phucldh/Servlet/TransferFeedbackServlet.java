/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Car.CarDAO;
import phucldh.Car.CarDTO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.Feedback.FeedbackDAO;
import phucldh.Feedback.FeedbackDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrderDetail.UserOrderDetailDAO;
import phucldh.UserOrderDetail.UserOrderDetailDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "TransferFeedbackServlet", urlPatterns = {"/TransferFeedbackServlet"})
public class TransferFeedbackServlet extends HttpServlet {

    private final String FEEDBACK_PAGE = "feedbackPage";
    private final String HISTORY_DETAIL_PAGE = "showHistoryDetailPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = HISTORY_DETAIL_PAGE;
        String id = request.getParameter("txtCarID");
        String orderLine = request.getParameter("txtOrderDetailID");
        boolean error = false;
        String errorMsg = "You can't send feedback if you haven't finish the order";

        try {
            HttpSession session = request.getSession(false);
            session.setAttribute("CARCATEFEEDBACK", null);
            session.setAttribute("CARFEEDBACK", null);
            session.setAttribute("ORDERLINEID", null);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            request.setAttribute("STATUSFEEDBACKERROR", null);
                            UserOrderDetailDAO daoUserOrderDetail = new UserOrderDetailDAO();
                            if (orderLine != null && !orderLine.equalsIgnoreCase("") && orderLine.length() > 0) {
                                UserOrderDetailDTO dtoUserDetail = daoUserOrderDetail.getUserOrderDetail(orderLine);
                                if (dtoUserDetail != null) {
                                    UserOrderDAO daoUserOrder = new UserOrderDAO();
                                    String status = daoUserOrder.getStatusOrder(dtoUserDetail.getOrderID());
                                    if (status.equalsIgnoreCase("Fi")) {
                                        CarDAO daoCar = new CarDAO();
                                        CarDTO dtoCar = daoCar.getCarByCarID(id);
                                        CarCategoryDAO daoCarCategory = new CarCategoryDAO();
                                        CarCategoryDTO dtoCarCategory = daoCarCategory.getCarCate(dtoCar.getCateCarID());
                                        String cusID = dtoAccount.getEmail();
                                        FeedbackDAO daoFeedback = new FeedbackDAO();
                                        FeedbackDTO checkDTO = daoFeedback.checkFeedbackExist(cusID, orderLine);
                                        if (checkDTO != null) {
                                            error = true;
                                            errorMsg = "You have been send feedback this order detail";
                                        } else if (checkDTO == null) {
                                            session.setAttribute("CARCATEFEEDBACK", dtoCarCategory);
                                            session.setAttribute("CARFEEDBACK", dtoCar);
                                            session.setAttribute("ORDERLINEID", orderLine);
                                            url = FEEDBACK_PAGE;
                                        }
                                    } else {
                                        error = true;
                                    }
                                    if (error) {
                                        request.setAttribute("STATUSFEEDBACKERROR", errorMsg);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("TransferFeedbackServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("TransferFeedbackServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("TransferFeedbackServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
