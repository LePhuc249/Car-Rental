/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.Cart.CartObject;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "UpdateCartServlet", urlPatterns = {"/UpdateCartServlet"})
public class UpdateCartServlet extends HttpServlet {

    private final String VIEW_CART_PAGE = "viewCartPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = VIEW_CART_PAGE;
        String errorContent = "You can't rent over 999 car";
        boolean error = false;
        CarCategoryDTO dtoCarCate = null;

        try {
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            CarCategoryDAO daoCarCategory = new CarCategoryDAO();
                            CartObject shoppingCart = (CartObject) session.getAttribute("shoppingCart");
                            String[] listID = request.getParameterValues("txtItemID");
                            String[] listQuantity = request.getParameterValues("txtQuantity");
                            for (int i = 0; i < listID.length; i++) {
                                int storeQuantity = daoCarCategory.getTotalQuantityItemByID(listID[i]);
                                String numberQuantity = listQuantity[i];
                                if (numberQuantity.length() >= 5) {
                                    error = true;
                                    if (error) {
                                        url = VIEW_CART_PAGE;
                                        request.setAttribute("UPDATER", errorContent);
                                    }
                                }
                                if (numberQuantity.length() < 5) {
                                    int buyQuantity = Integer.parseInt(listQuantity[i]);
                                    if (buyQuantity > storeQuantity) {
                                        error = true;
                                        errorContent = "The current quantity of " + daoCarCategory.getNameByID(listID[i]) + " in store: " + storeQuantity;
                                    }
                                    if (error) {
                                        url = VIEW_CART_PAGE;
                                        request.setAttribute("UPDATER", errorContent);
                                    } else {
                                        shoppingCart.update(listID[i], buyQuantity);
                                        session.setAttribute("shoppingCart", shoppingCart);
                                        url = VIEW_CART_PAGE;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("UpdateCartServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("UpdateCartServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("UpdateCartServlet_NullPointer " + ex.getMessage());
        } catch (Exception ex) {
            log("UpdateCartServlet_Exception " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
