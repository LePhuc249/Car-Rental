/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Car.CarDAO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;
import phucldh.UserOrderDetail.UserOrderDetailDAO;
import phucldh.UserOrderDetail.UserOrderDetailDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SearchHistoryServlet", urlPatterns = {"/SearchHistoryServlet"})
public class SearchHistoryServlet extends HttpServlet {

    private final String HISTORY_PAGE = "showHistoryPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = HISTORY_PAGE;
        String inputDate = request.getParameter("txtShoppingDate");
        String txtSearchName = request.getParameter("txtSearchName");

        try {
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            session.setAttribute("SEARCHDATEHISTORY", null);
                            UserOrderDAO daoUserOrder = new UserOrderDAO();
                            UserOrderDetailDAO daoUserOrderDetail = new UserOrderDetailDAO();
                            CarCategoryDAO daoCarCategory = new CarCategoryDAO();
                            CarDAO daoCar = new CarDAO();
                            Map<UserOrderDTO, List<UserOrderDetailDTO>> mapHistory = new LinkedHashMap<>();
                            daoUserOrder.getListHistory(dtoAccount.getEmail());
                            List<UserOrderDTO> list = daoUserOrder.ListHistory();
                            if (list != null) {
                                if (inputDate != null && inputDate.length() > 0 && (txtSearchName == null || txtSearchName.length() == 0 || txtSearchName.equalsIgnoreCase(""))) {
                                    List<UserOrderDTO> resultList = new ArrayList<>();
                                    for (UserOrderDTO userOrderDTO : list) {
                                        if (userOrderDTO.getOrderDate().toString().substring(0, 10).equalsIgnoreCase(inputDate)) {
                                            resultList.add(userOrderDTO);
                                        }
                                    }
                                    for (UserOrderDTO userOrderDTO : resultList) {
                                        daoUserOrderDetail.getItem(userOrderDTO.getOrderID());
                                        List<UserOrderDetailDTO> listDetail = daoUserOrderDetail.ListItemInOrder();
                                        mapHistory.put(userOrderDTO, listDetail);
                                    }
                                    session.setAttribute("SEARCHDATEHISTORY", inputDate);
                                }
                                if (txtSearchName != null && txtSearchName.length() > 0 && !txtSearchName.equalsIgnoreCase("") && (inputDate == null || inputDate.length() == 0)) {
                                    List<String> listOrderID = new ArrayList<>();
                                    List<String> listAfter = new ArrayList<>();
                                    List<String> filer = new ArrayList<>();
                                    List<UserOrderDTO> newList = new ArrayList<>();
                                    for (UserOrderDTO userOrderDTO : list) {
                                        daoUserOrderDetail.getItemByOrderIDAndDetail(userOrderDTO.getOrderID(), txtSearchName);
                                        List<UserOrderDetailDTO> resultList = daoUserOrderDetail.ListItemInSearch();
                                        if (resultList.size() > 0) {
                                            for (UserOrderDetailDTO userOrderDetailDTO : resultList) {
                                                listOrderID.add(userOrderDetailDTO.getOrderID());
                                            }
                                            for (String element : listOrderID) {
                                                if (!listAfter.contains(element)) {
                                                    listAfter.add(element);
                                                }
                                            }
                                        }
                                    }
                                    for (String string : listAfter) {
                                        if (!filer.contains(string)) {
                                            filer.add(string);
                                        }
                                    }
                                    for (String string : listAfter) {
                                        UserOrderDTO dto = daoUserOrder.getOrder(string);
                                        newList.add(dto);
                                    }
                                    for (UserOrderDTO userOrderDTO : newList) {
                                        daoUserOrderDetail.getItem(userOrderDTO.getOrderID());
                                        List<UserOrderDetailDTO> listDetail = daoUserOrderDetail.ListItemInOrder();
                                        mapHistory.put(userOrderDTO, listDetail);
                                    }
                                }
                                if (inputDate != null && inputDate.length() > 0 && txtSearchName != null && txtSearchName.length() > 0 && !txtSearchName.equalsIgnoreCase("")) {
                                    List<UserOrderDTO> resultList = new ArrayList<>();
                                    List<String> listOrderID = new ArrayList<>();
                                    List<String> listAfter = new ArrayList<>();
                                    List<String> filer = new ArrayList<>();
                                    List<UserOrderDTO> newList = new ArrayList<>();
                                    for (UserOrderDTO userOrderDTO : list) {
                                        if (userOrderDTO.getOrderDate().toString().substring(0, 10).equalsIgnoreCase(inputDate)) {
                                            resultList.add(userOrderDTO);
                                        }
                                    }
                                    for (UserOrderDTO userOrderDTO : resultList) {
                                        daoUserOrderDetail.getItemByOrderIDAndDetail(userOrderDTO.getOrderID(), txtSearchName);
                                        List<UserOrderDetailDTO> listOrder = daoUserOrderDetail.ListItemInSearch();
                                        if (listOrder.size() > 0) {
                                           for (UserOrderDetailDTO userOrderDetailDTO : listOrder) {
                                                listOrderID.add(userOrderDetailDTO.getOrderID());
                                            }
                                            for (String element : listOrderID) {
                                                if (!listAfter.contains(element)) {
                                                    listAfter.add(element);
                                                }
                                            }
                                        }
                                    }
                                    for (String string : listAfter) {
                                        if (!filer.contains(string)) {
                                            filer.add(string);
                                        }
                                    }
                                    for (String string : listAfter) {
                                        UserOrderDTO dto = daoUserOrder.getOrder(string);
                                        newList.add(dto);
                                    }
                                    for (UserOrderDTO userOrderDTO : newList) {
                                        daoUserOrderDetail.getItem(userOrderDTO.getOrderID());
                                        List<UserOrderDetailDTO> listDetail = daoUserOrderDetail.ListItemInOrder();
                                        mapHistory.put(userOrderDTO, listDetail);
                                    }
                                    session.setAttribute("SEARCHDATEHISTORY", inputDate);
                                }
                                request.setAttribute("HISTORYUSERORDER", mapHistory);
                                CouponsDAO daoCoupons = new CouponsDAO();
                                daoCoupons.getListCoupons();
                                List<CouponsDTO> listCoupons = daoCoupons.getTheList();
                                session.setAttribute("LISTCOUPONSTOCHECK", listCoupons);
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("SearchHistoryServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("SearchHistoryServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("SearchHistoryServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
