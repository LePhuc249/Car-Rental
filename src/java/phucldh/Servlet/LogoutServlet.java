/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "LogoutServlet", urlPatterns = {"/LogoutServlet"})
public class LogoutServlet extends HttpServlet {

    private final String LOGIN_PAGE = "loginPage";
    private final int RECORD_PER_PAGE = 20;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = LOGIN_PAGE;
        int page = 1;
        int noOfRecord = 0;
        String pageParam = request.getParameter("page");

        try {
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(false);
            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }
            if (session != null) {
                AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
                if (dtoAccount != null) {
                    session.setAttribute("ACCOUNTOBJECT", null);
                    session.setAttribute("ROLENAME", null);
                    Cookie[] cookies = request.getCookies();
                    if (cookies != null) {
                        for (Cookie cooky : cookies) {
                            cooky.setMaxAge(0);
                            response.addCookie(cooky);
                        }
                    }
                    session.invalidate();
                    url = LOGIN_PAGE;
                    context.setAttribute("ITEMLIST", null);
                    session = request.getSession(true);
                    CarCategoryDAO daoCarCategory = new CarCategoryDAO();
                    daoCarCategory.getCarCateFull((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                    List<CarCategoryDTO> listCar = daoCarCategory.getListCategoryAll();
                    context.setAttribute("ITEMLIST", listCar);
                    noOfRecord = daoCarCategory.getCounterCarCateFull();
                    if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                        int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                        session.setAttribute("NOOFPAGE", noOfPage);
                        session.setAttribute("CURRENTPAGE", page);
                    }
                }
            } else if (session == null) {
                url = LOGIN_PAGE;
            }
        } catch (SQLException ex) {
            log("LogoutServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("LogoutItemServlet_Naming " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("LogoutItemServlet_NullPointer " + ex.getMessage());
        } finally {
            response.sendRedirect(url);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
