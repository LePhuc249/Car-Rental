/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.GooglereCAPTCHA.VerifyRecaptcha;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    private final int RECORD_PER_PAGE = 20;
    private final String INVALID_LOGIN = "invalidLoginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String VERIFY_PAGE = "VerifyCodePage";
    private final String INVALID_RECAPTCHA = "invalidReCaptchaPage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = INVALID_LOGIN;
        String userEmail = request.getParameter("txtUserEmail");
        String password = request.getParameter("txtPassword");
        String pageParam = request.getParameter("page");
        int page = 1;
        int noOfRecord = 0;

        try {
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(true);
            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }
            AccountDAO daoAccount = new AccountDAO();
            AccountDTO dtoAccount = daoAccount.getAccount(userEmail, password);
            CarCategoryDAO daoCarCategory = new CarCategoryDAO();
            if (dtoAccount != null) {
                String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
                boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
                if (verify) {
                    String roleID = daoAccount.getRole(dtoAccount.getEmail());
                    if (roleID != null && !roleID.equals("")) {
                        RoleDAO daoRole = new RoleDAO();
                        String roleName = daoRole.getRoleName(roleID);
                        if (roleName != null && !roleName.equals("")) {
                            if (roleName.equalsIgnoreCase("Admin")) {
                                url = ADMIN_PAGE;
                            } else if (roleName.equalsIgnoreCase("Customer")) {
                                if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                    url = VERIFY_PAGE;
                                } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                    url = CUSTOMER_PAGE;
                                }
                            }
                            Cookie usernameCookie = new Cookie("USEREMAIL", userEmail);
                            usernameCookie.setMaxAge(60 * 3);
                            response.addCookie(usernameCookie);
                            Cookie passwordCookie = new Cookie("PASSWORD", password);
                            passwordCookie.setMaxAge(60 * 3);
                            response.addCookie(passwordCookie);
                            session.setAttribute("ACCOUNTOBJECT", dtoAccount);
                            session.setAttribute("ROLENAME", roleName);
                        }
                    }
                } else {
                    url = INVALID_RECAPTCHA;
                }
            }
        } catch (NamingException ex) {
            log("LoginServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("LoginServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("LoginServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("LoginServlet_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
