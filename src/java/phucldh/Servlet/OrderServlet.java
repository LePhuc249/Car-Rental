/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Car.CarDAO;
import phucldh.CarCategory.CarCategoryDAO;
import phucldh.CarCategory.CarCategoryDTO;
import phucldh.Cart.CartObject;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;
import phucldh.UserOrderDetail.UserOrderDetailDAO;
import phucldh.UserOrderDetail.UserOrderDetailDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "OrderServlet", urlPatterns = {"/OrderServlet"})
public class OrderServlet extends HttpServlet {

    private final String VIEW_CART_PAGE = "viewCartPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        String url = VIEW_CART_PAGE;
        String rentalDate = request.getParameter("txtDateWantRental");
        String returnDate = request.getParameter("txtDateWantReturn");
        String couponsCode = request.getParameter("txtCouponCode");
        boolean error = false;
        String errorContent = "";
        String totalNow = request.getParameter("txtTotalPrice");

        try {
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            rentalDate = (String) session.getAttribute("VALUETRANSFERDATERENTAL");
                            returnDate = (String) session.getAttribute("VALUETRANSFERDATERETURN");
                            couponsCode = (String) session.getAttribute("VALUETRANSFERCOUPONSCODE");
                            totalNow = (String) session.getAttribute("VALUETRANSFERTOTALPRICE");
                            String userID = dtoAccount.getEmail();
                            UserOrderDAO daoUserOrder = new UserOrderDAO();
                            CouponsDAO daoCoupons = new CouponsDAO();
                            CarDAO daoCar = new CarDAO();
                            CarCategoryDAO daoCarCategory = new CarCategoryDAO();
                            UserOrderDetailDAO daoUserOrderDetail = new UserOrderDetailDAO();
                            String orderID = null;
                            CouponsDTO dtoCoupons = null;
                            orderID = daoUserOrder.getLastOrderByIDByDate();
                            if (orderID == null) {
                                orderID = "O_1";
                            } else if (orderID != null) {
                                String numberCount = orderID.substring(2);
                                int count = Integer.parseInt(numberCount);
                                count = count + 1;
                                orderID = "O_" + count;
                            }
                            String statusDefault = "Pr";
                            if (!couponsCode.equalsIgnoreCase("")) {
                                dtoCoupons = daoCoupons.getCouponsByCode(couponsCode);
                                if (dtoCoupons != null) {
                                    String couponsID = dtoCoupons.getCouponsID();
                                    daoUserOrder.getListCoupons();
                                    List<String> listCouponsHaveUsed = daoUserOrder.ListCouponsHaveUsed();
                                    if (listCouponsHaveUsed != null) {
                                        for (String string : listCouponsHaveUsed) {
                                            if (string != null) {
                                                if (string.equalsIgnoreCase(couponsID)) {
                                                    error = true;
                                                    errorContent = "The coupon code have been used!!!!";
                                                }
                                            }
                                        }
                                    }
                                    if (error) {
                                        request.setAttribute("ERRORCHECKCOUPONS", errorContent);
                                    } else {
                                        CartObject shoppingCart = (CartObject) session.getAttribute("shoppingCart");
                                        float beforeTotal = Float.parseFloat(totalNow);
                                        float discountTotal = (beforeTotal * dtoCoupons.getDiscountAmount()) / 100;
                                        float total = beforeTotal - discountTotal;
                                        UserOrderDTO dtoNewOrder = new UserOrderDTO(orderID, userID, rentalDate, returnDate, statusDefault, total, couponsID);
                                        boolean createNewOrder = daoUserOrder.createNewOrder(dtoNewOrder);
                                        if (createNewOrder) {
                                            String lastOrderDetaiID = daoUserOrderDetail.getLastOrderDetailByIDByDate();
                                            for (CarCategoryDTO value : shoppingCart.getShoppingCart().values()) {
                                                int numberCarRent = value.getQuantity();
                                                List<String> ListIdCarCanRent = new ArrayList<>();
                                                for (int i = 0; i < numberCarRent; i++) {
                                                    String id = daoCar.getCarFree(value.getCateID());
                                                    daoCar.updateStatusOfCarToBusy(id);
                                                    ListIdCarCanRent.add(id);
                                                }
                                                for (String string : ListIdCarCanRent) {
                                                    if (lastOrderDetaiID == null) {
                                                        lastOrderDetaiID = "OL_1";
                                                    } else if (lastOrderDetaiID != null) {
                                                        String numberCount = lastOrderDetaiID.substring(3);
                                                        int count = Integer.parseInt(numberCount);
                                                        count = count + 1;
                                                        lastOrderDetaiID = "OL_" + count;
                                                    }
                                                    String carProperty = "";
                                                    String carCategoryID = daoCar.getCarCategory(string);
                                                    CarCategoryDTO dtoCarCatoryNow = daoCarCategory.getCarCate(carCategoryID);
                                                    carProperty = dtoCarCatoryNow.getCateModel() + "_" + dtoCarCatoryNow.getCateColor() + "_" + dtoCarCatoryNow.getCateYear() + "_" + dtoCarCatoryNow.getCateMake();
                                                    UserOrderDetailDTO dtoDetail = new UserOrderDetailDTO(lastOrderDetaiID, orderID, string, carProperty);
                                                    daoUserOrderDetail.createOrderDetail(dtoDetail);
                                                }
                                                url = CUSTOMER_PAGE;
                                                shoppingCart = null;
                                                session.setAttribute("shoppingCart", shoppingCart);
                                                session.setAttribute("COUPONSDTOINPUT", null);
                                            }
                                        } else {
                                            error = true;
                                            errorContent = "There having error to order Please try again!!!!";
                                        }
                                        if (error) {
                                            request.setAttribute("ERRORORDER", errorContent);
                                        }
                                    }
                                }
                            } else {
                                CartObject shoppingCart = (CartObject) session.getAttribute("shoppingCart");
                                float total = Float.parseFloat(totalNow);
                                UserOrderDTO dtoNewOrder = new UserOrderDTO(orderID, userID, rentalDate, returnDate, statusDefault, total);
                                boolean createNewOrder = daoUserOrder.createNewOrderWithOutCoupons(dtoNewOrder);
                                if (createNewOrder) {
                                    String lastOrderDetaiID = daoUserOrderDetail.getLastOrderDetailByIDByDate();
                                    for (CarCategoryDTO value : shoppingCart.getShoppingCart().values()) {
                                        int numberCarRent = value.getQuantity();
                                        List<String> ListIdCarCanRent = new ArrayList<>();
                                        for (int i = 0; i < numberCarRent; i++) {
                                            String id = daoCar.getCarFree(value.getCateID());
                                            daoCar.updateStatusOfCarToBusy(id);
                                            ListIdCarCanRent.add(id);
                                        }
                                        for (String string : ListIdCarCanRent) {
                                            if (lastOrderDetaiID == null) {
                                                lastOrderDetaiID = "OL_1";
                                            } else if (lastOrderDetaiID != null) {
                                                String numberCount = lastOrderDetaiID.substring(3);
                                                int count = Integer.parseInt(numberCount);
                                                count = count + 1;
                                                lastOrderDetaiID = "OL_" + count;
                                            }
                                            String carProperty = "";
                                            String carCategoryID = daoCar.getCarCategory(string);
                                            CarCategoryDTO dtoCarCatoryNow = daoCarCategory.getCarCate(carCategoryID);
                                            carProperty = dtoCarCatoryNow.getCateModel() + "_" + dtoCarCatoryNow.getCateColor() + "_" + dtoCarCatoryNow.getCateYear() + "_" + dtoCarCatoryNow.getCateMake();
                                            UserOrderDetailDTO dtoDetail = new UserOrderDetailDTO(lastOrderDetaiID, orderID, string, carProperty);
                                            daoUserOrderDetail.createOrderDetail(dtoDetail);
                                        }
                                        url = CUSTOMER_PAGE;
                                        shoppingCart = null;
                                        session.setAttribute("shoppingCart", shoppingCart);
                                        session.setAttribute("COUPONSDTOINPUT", null);
                                    }
                                } else {
                                    error = true;
                                    errorContent = "There having error to order Please try again!!!!";
                                }
                                if (error) {
                                    request.setAttribute("ERRORORDER", errorContent);
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("OrderServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("OrderServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("OrderServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("OrderServlet_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
