/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import phucldh.Account.AccountDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CheckVerifyCodeAccount", urlPatterns = {"/CheckVerifyCodeAccount"})
public class CheckVerifyCodeAccount extends HttpServlet {

    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String VERIFY_PAGE = "VerifyCodePage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String codeInput = request.getParameter("txtVerifyCode");
        String accountID = request.getParameter("accID");
        String url = VERIFY_PAGE;
        boolean error = false;
        String errorVerifyCode = "You have input wrong! Please check it before input again";

        try {
            AccountDAO daoAccount = new AccountDAO();
            String codeVerify = daoAccount.getVerifyCodeByAccountID(accountID);
            if (!codeInput.equalsIgnoreCase(codeVerify)) {
                error = true;
            } else if (codeInput.equalsIgnoreCase(codeVerify)) {
                boolean updateStatus = daoAccount.updateStatusToActive(accountID);
                if (updateStatus) {
                    boolean clearCodeVerify = daoAccount.clearVerifyCodeByAccountID(accountID);
                    if (clearCodeVerify) {
                        url = CUSTOMER_PAGE;
                    }
                }
            }
            if (error) {
                request.setAttribute("ERRORVERIFY", errorVerifyCode);
            }
        } catch (NamingException ex) {
            log("CheckVerifyCodeAccountServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("CheckVerifyCodeAccountServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("CheckVerifyCodeAccountServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
