/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Car.CarDAO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;
import phucldh.UserOrderDetail.UserOrderDetailDAO;
import phucldh.UserOrderDetail.UserOrderDetailDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "DeleteOrderServlet", urlPatterns = {"/DeleteOrderServlet"})
public class DeleteOrderServlet extends HttpServlet {

    private final String HISTORY_PAGE = "showHistoryPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final int RECORD_PER_PAGE = 20;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = HISTORY_PAGE;
        String id = request.getParameter("txtOrderID");
        boolean error = false;
        String errorCT = null;
        int page = 1;
        int noOfRecord = 0;
        String pageParam = request.getParameter("page");
        String nameSearch = request.getParameter("valueSearchName");
        String dateSearch = request.getParameter("valueSearchDate");

        try {
            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getEmail());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("errorROLE", null);
                            request.setAttribute("errorROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            UserOrderDAO daoUserOrder = new UserOrderDAO();
                            UserOrderDTO dtoUserOrder = daoUserOrder.getOrder(id);
                            String orderStatus = dtoUserOrder.getStatus();
                            if (orderStatus.equalsIgnoreCase("Hi")) {
                                request.setAttribute("ERRORCT", null);
                                error = true;
                                errorCT = "The current status is hiring so you can't delete order please wait uttil order finish";
                                request.setAttribute("ERRORCT", errorCT);
                            } else if (orderStatus.equalsIgnoreCase("Fi")) {
                                request.setAttribute("ERRORCT", null);
                                error = true;
                                errorCT = "The current status is finish so you can't delete order please wait uttil order finish";
                                request.setAttribute("ERRORCT", errorCT);
                            } else if (orderStatus.equalsIgnoreCase("Ca")) {
                                request.setAttribute("ERRORCT", null);
                                error = true;
                                errorCT = "The current status is cancel so you can't delete order";
                                request.setAttribute("ERRORCT", errorCT);
                            } else if (orderStatus.equalsIgnoreCase("Pr") || orderStatus.equalsIgnoreCase("Co")) {
                                boolean updateStatus = daoUserOrder.updateStatusToCancel(id);
                                if (updateStatus) {
                                    UserOrderDetailDAO daoUserOrderDetail = new UserOrderDetailDAO();
                                    daoUserOrderDetail.getItem(id);
                                    List<UserOrderDetailDTO> listOrderDetails = daoUserOrderDetail.ListItemInOrder();
                                    for (UserOrderDetailDTO listOrderDetail : listOrderDetails) {
                                        String carID = listOrderDetail.getCarID();
                                        CarDAO daoCar = new CarDAO();
                                        boolean updateStatusCar = daoCar.updateStatusOfCarToFree(carID);
                                        if (updateStatusCar) {
                                            url = "SearchHistoryServlet?txtShoppingDate=" + dateSearch + "&txtSearchName=" + nameSearch
                                                    +  "&btAction=SearchHistoryBuy";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("DeleteOrderServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("DeleteOrderServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("DeleteOrderServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
