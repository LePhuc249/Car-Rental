/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Coupons;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class CouponsDTO implements Serializable{
    private String couponsID;
    private String couponsCode;
    private int discountAmount;
    private Date expirationDate;
    private Date createDate;
    private boolean isDelete;

    public CouponsDTO() {
    }

    public CouponsDTO(String couponsID, String couponsCode, int discountAmount, Date expirationDate, boolean isDelete) {
        this.couponsID = couponsID;
        this.couponsCode = couponsCode;
        this.discountAmount = discountAmount;
        this.expirationDate = expirationDate;
        this.isDelete = isDelete;
    }

    public CouponsDTO(String couponsID, String couponsCode, int discountAmount, Date expirationDate, Date createDate, boolean isDelete) {
        this.couponsID = couponsID;
        this.couponsCode = couponsCode;
        this.discountAmount = discountAmount;
        this.expirationDate = expirationDate;
        this.createDate = createDate;
        this.isDelete = isDelete;
    }

    public String getCouponsID() {
        return couponsID;
    }

    public void setCouponsID(String couponsID) {
        this.couponsID = couponsID;
    }

    public String getCouponsCode() {
        return couponsCode;
    }

    public void setCouponsCode(String couponsCode) {
        this.couponsCode = couponsCode;
    }

    public int getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(int discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
    
    
    
}
