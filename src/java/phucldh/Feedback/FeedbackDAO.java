/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Feedback;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class FeedbackDAO implements Serializable{
    public boolean createFeedback(FeedbackDTO dtoFeedback) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO Feedback (FeedbackID, accountID, orderLineID, feedbackContent, counter, status) "
                        + "VALUES (?,?,?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoFeedback.getFeedbackID());
                stm.setString(2, dtoFeedback.getAccountID());
                stm.setString(3, dtoFeedback.getOrderLineID());
                stm.setString(4, dtoFeedback.getFeedbackContent());
                stm.setInt(5, dtoFeedback.getCounter());
                stm.setString(6, dtoFeedback.getStatus());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public String getLastFeedbackID() throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String id = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT FeedbackID FROM Feedback WHERE feedbackTime = (SELECT MAX(feedbackTime) FROM Feedback)";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    id = rs.getString("FeedbackID");
                }
                return id;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public FeedbackDTO checkFeedbackExist(String customerID, String orderLineID) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        FeedbackDTO dto = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT FeedbackID, accountID, orderLineID, feedbackContent, counter, status"
                        + " FROM Feedback WHERE accountID = ? AND orderLineID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, customerID);
                stm.setString(2, orderLineID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("FeedbackID");
                    String cusId = rs.getString("accountID");
                    String orderLID = rs.getString("orderLineID");
                    String comment = rs.getString("feedbackContent");
                    int quanlity = rs.getInt("counter");
                    String status = rs.getString("status");
                    dto = new FeedbackDTO(id, cusId, orderLID, comment, quanlity, status);
                }
                return dto;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
}
