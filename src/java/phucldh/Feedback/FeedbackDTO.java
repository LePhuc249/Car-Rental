/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Feedback;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class FeedbackDTO implements Serializable {

    private String feedbackID;
    private String accountID;
    private String orderLineID;
    private Date feedbackTime;
    private String feedbackContent;
    private int counter;
    private String status;

    public FeedbackDTO() {
    }

    public FeedbackDTO(String feedbackID, String accountID, String orderLineID, String feedbackContent, int counter, String status) {
        this.feedbackID = feedbackID;
        this.accountID = accountID;
        this.orderLineID = orderLineID;
        this.feedbackContent = feedbackContent;
        this.counter = counter;
        this.status = status;
    }

    public FeedbackDTO(String feedbackID, String accountID, String orderLineID, Date feedbackTime, String feedbackContent, int counter, String status) {
        this.feedbackID = feedbackID;
        this.accountID = accountID;
        this.orderLineID = orderLineID;
        this.feedbackTime = feedbackTime;
        this.feedbackContent = feedbackContent;
        this.counter = counter;
        this.status = status;
    }

    public String getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(String feedbackID) {
        this.feedbackID = feedbackID;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getOrderLineID() {
        return orderLineID;
    }

    public void setOrderLineID(String orderLineID) {
        this.orderLineID = orderLineID;
    }

    public Date getFeedbackTime() {
        return feedbackTime;
    }

    public void setFeedbackTime(Date feedbackTime) {
        this.feedbackTime = feedbackTime;
    }

    public String getFeedbackContent() {
        return feedbackContent;
    }

    public void setFeedbackContent(String feedbackContent) {
        this.feedbackContent = feedbackContent;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
