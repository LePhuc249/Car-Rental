/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.CarStatus;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class CarStatusDTO implements Serializable{
    
    private String carStatusID;
    private String carStatusContent;
    private Date createDate;
    private boolean isDelete;

    public CarStatusDTO() {
    }

    public CarStatusDTO(String carStatusID, String carStatusContent) {
        this.carStatusID = carStatusID;
        this.carStatusContent = carStatusContent;
    }

    public CarStatusDTO(String carStatusID, boolean isDelete) {
        this.carStatusID = carStatusID;
        this.isDelete = isDelete;
    }
    
    public CarStatusDTO(String carStatusID, String carStatusContent, boolean isDelete) {
        this.carStatusID = carStatusID;
        this.carStatusContent = carStatusContent;
        this.isDelete = isDelete;
    }

    public CarStatusDTO(String carStatusID, String carStatusContent, Date createDate, boolean isDelete) {
        this.carStatusID = carStatusID;
        this.carStatusContent = carStatusContent;
        this.createDate = createDate;
        this.isDelete = isDelete;
    }

    public String getCarStatusID() {
        return carStatusID;
    }

    public void setCarStatusID(String carStatusID) {
        this.carStatusID = carStatusID;
    }

    public String getCarStatusContent() {
        return carStatusContent;
    }

    public void setCarStatusContent(String carStatusContent) {
        this.carStatusContent = carStatusContent;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

}
