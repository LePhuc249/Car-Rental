/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Role;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class RoleDAO implements Serializable{
    public RoleDTO getRoleObject(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select roleID, roleName From "
                        + "Role Where roleID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String roleID = rs.getString("roleID");
                    String roleName = rs.getString("roleName");
                    RoleDTO dtoRole = new RoleDTO(roleID, roleName);
                    return dtoRole;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public boolean createNewRole(RoleDTO dtoRole) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Insert into Role (roleID, roleName) "
                        + "Values (?, ?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoRole.getRoleID());
                stm.setString(2, dtoRole.getRoleName());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public String getRoleName(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select roleName From "
                        + "Role Where roleID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String roleName = rs.getString("roleName");
                    return roleName;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
}
